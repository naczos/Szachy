/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.swing.*;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.*;
import static java.lang.Math.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.text.NumberFormat;
import javax.swing.border.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
/**
 *
 * @author Marcin
 */
public class Pierwsze extends JFrame
{
    JButton Start;
    JLabel J1l, J2l, N1l, N2l, b1l, b2l, kl,
            amplitudal, CzasSymulacjil, CzasKrokul, okresl;
    JTextField J1t, J2t, N1t, N2t, b1t, b2t, kt, 
            tAmplituda, tCzasSymulacji, 
            tCzasKroku, tokres;
    JCheckBox checkPobudzenie, checkWalec1, checWalec2;
    JRadioButton radioProstokat, radioTrojkat, radioHarmoniczny,
            radioImpuls, radioSkok, radioEuler, radioRK;
    
    JPanel thePanel, wyswietlaniePanel, Wykres, sygnalyPanel;
    XYSeries wykresPobudzenie, wykresKat1, wykresKat2;
    ChartPanel CPWykres;
    
    
   public static void main(String [] args)
   {  
       new Pierwsze();    
   }

   
   public Pierwsze()
   {    
       this.setSize(1000,800); //Tworzymy rozmiar okna
       
       this.setLocationRelativeTo(null); //ustawiamy okno na środku ekranu
       this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // ustawiamy że po kliknięciu krzyżyka progarm się wyłącza
       this.setTitle("Projek MMM"); // ustawiamy tytuł okna
        
       Wykres = new JPanel(); // ustawienie panelu w którym bęzdie wykres
        
       thePanel = new JPanel(); //ustawiamy główny panel
        
        
       Start = new JButton("start"); //Dodajemy przycisk który będzie rozpoczynał symulacje
       sluchajPrzycisk Startujemy = new sluchajPrzycisk();
       Start.addActionListener(Startujemy);
       thePanel.add(Start); //dodajemy przycisk start do głównego panelu
        
        //Dodajemy CheckBoxy
        checkPobudzenie = new JCheckBox("Pobudzenie");
        checkWalec1 = new  JCheckBox("Kat obrotu pierwszego walca");
        checWalec2 = new JCheckBox("Kąt obrotu drugiego walca");
        checWalec2.setSelected(true);
        checkWalec1.setSelected(true);
        checkPobudzenie.setSelected(true);
        
        thePanel.add(checkPobudzenie);
        thePanel.add(checkWalec1);
        thePanel.add(checWalec2);
        //tworzenie panelu dla Checkboxów
        wyswietlaniePanel = new JPanel();
        //Utworzenie specjalnej ramki dla checkBoxów
        Border wyswietlanieBorder = BorderFactory.createTitledBorder("Wyświetlaj");
        //Dodanie tej ramki do panelu sygnaly
        wyswietlaniePanel.setBorder(wyswietlanieBorder);
        //dodawanie checkbooksów do panelu wyświetlanie
        wyswietlaniePanel.add(checkPobudzenie);
        wyswietlaniePanel.add(checkWalec1);
        wyswietlaniePanel.add(checWalec2);
        thePanel.add(wyswietlaniePanel);
         // koniec dodawania CheckBoxów
          
        //Dodajemy Radio do rodzajów sygnału
        radioProstokat = new JRadioButton("Prostokątny");
        radioTrojkat = new JRadioButton("Trójkątny");
        radioHarmoniczny = new JRadioButton("Harmoniczny");
        radioImpuls = new JRadioButton("Impuls");
        radioSkok = new JRadioButton("Skok");
        
        ButtonGroup sygnaly = new ButtonGroup(); //umieszczenie wszystkich sygnałów w jednej grupie
        //Dodanie poszczególnych sygnałów do grupy sygnały
        sygnaly.add(radioProstokat);
        sygnaly.add(radioTrojkat);
        sygnaly.add(radioHarmoniczny);
        sygnaly.add(radioImpuls);
        sygnaly.add(radioSkok);
        //Utworzenie specjalnego panelu dla sygnałów
        sygnalyPanel = new JPanel();
	//Utworzenie specjalnej ramki dla sygnałów
        Border sygnalyBorder = BorderFactory.createTitledBorder("Sygnały");
        //Dodanie tej ramki do panelu sygnaly
        sygnalyPanel.setBorder(sygnalyBorder);
        // Dodawanie przycisków do panelu sygnały
        sygnalyPanel.add(radioProstokat);
        sygnalyPanel.add(radioTrojkat);
        sygnalyPanel.add(radioHarmoniczny);
        sygnalyPanel.add(radioImpuls);
        sygnalyPanel.add(radioSkok);
        //ustawienie Skoku jako domyślny
        radioSkok.setSelected(true);
        //Dodam tez pola tekstowe amlplitudal, CzasSymulacjil, CzasKrokul, okres;
        amplitudal = new JLabel("Amplituda");			
	sygnalyPanel.add(amplitudal);										
	tAmplituda = new JTextField("1", 3);							
	sygnalyPanel.add(tAmplituda);
        
        CzasSymulacjil = new JLabel("Czas symulacji");			
	sygnalyPanel.add(CzasSymulacjil);										
	tCzasSymulacji = new JTextField("50", 3);							
	sygnalyPanel.add(tCzasSymulacji);
        
        CzasKrokul = new JLabel("Czas kroku");			
	sygnalyPanel.add(CzasKrokul);										
	tCzasKroku = new JTextField("0.1", 3);							
	sygnalyPanel.add(tCzasKroku);
        
        okresl = new JLabel("Okres");			
	sygnalyPanel.add(okresl);										
	tokres = new JTextField("20", 3);							
	sygnalyPanel.add(tokres);
        //Dodanie panelu sygnały do głównego panelu
        thePanel.add(sygnalyPanel);
        
        //Dodajemy Radio do rodzajów symulacji
        radioEuler = new JRadioButton("Euler");
        radioRK = new JRadioButton("RK");
        
        //umieszczenie wszystkich sygnałów w jednej grupie
        ButtonGroup symulacja = new ButtonGroup(); 
        //Dodanie poszczególnych sygnałów do grupy sygnały
        symulacja.add(radioEuler);
        symulacja.add(radioRK);
        
        //Utworzenie specjalnego panelu dla symulacja
        JPanel symulacjaPanel = new JPanel();
	//Utworzenie specjalnej ramki dla symulacja
        Border symulacjaBorder = BorderFactory.createTitledBorder("Rodzaje symulacji");
        //Dodanie tej ramki do panelu symulacja
        symulacjaPanel.setBorder(symulacjaBorder);
        // Dodawanie przycisków do panelu symulacja
        symulacjaPanel.add(radioEuler);
        symulacjaPanel.add(radioRK);
        //ustawienie euler jako domyślny
        radioEuler.setSelected(true);
        //Dodanie panelu symulacja do głównego panelu
        thePanel.add(symulacjaPanel);
        
        JPanel technicznePanel = new JPanel();
        Border technicznaBorder = BorderFactory.createTitledBorder("Parametry techniczne");
        //Dodanie tej ramki do panelu sygnaly
        technicznePanel.setBorder(technicznaBorder);
        J1l = new JLabel("J1");			
	technicznePanel.add(J1l);										
	J1t = new JTextField("1", 3);							
	technicznePanel.add(J1t);
        
        J2l = new JLabel("J2");			
	technicznePanel.add(J2l);										
	J2t = new JTextField("2", 3);							
	technicznePanel.add(J2t);
        
        N1l = new JLabel("N1");			
	technicznePanel.add(N1l);										
	N1t = new JTextField("3", 3);							
	technicznePanel.add(N1t);
        
        N2l = new JLabel("N2");			
	technicznePanel.add(N2l);										
	N2t = new JTextField("2", 3);							
	technicznePanel.add(N2t);
        
        b1l = new JLabel("b1");			
	technicznePanel.add(b1l);										
	b1t = new JTextField("1", 3);							
	technicznePanel.add(b1t);
        
        b2l = new JLabel("b2");			
	technicznePanel.add(b2l);										
	b2t = new JTextField("2", 3);							
	technicznePanel.add(b2t);
        
        kl = new JLabel("k");			
	technicznePanel.add(kl);										
	kt = new JTextField("2", 3);							
	technicznePanel.add(kt);
        
        thePanel.add(technicznePanel);
        
        thePanel.add(Wykres);
         
        this.add(thePanel); // dodajemy główny panel do okna
        this.setVisible(true); // ustawiamy okno jako widoczne
  
   }//koniec public pierwsze
   
   private class sluchajPrzycisk implements ActionListener
   {
			
	public void actionPerformed(ActionEvent e)
        {	
            boolean wszystkoOK=true;
            if(e.getSource() == Start)
            {
  
		double pobudz=0 // pobudzenie
                , kat1 = 0 //kąt pierwszy
                , kat2 = 0 //kąt drugi
                , poch_kat1 = 0 //pochodna kąta pierwszego
                , czas=100 // czas trwania symulacji
                , krok = 0.001 //czas trwania kroku symulacji
                , metoda = 2; // wybór metody 1 to E, 2 to Rungego-Kutty
                int rodzaj_sygnalu=3; //wybór rodzaju sygnału np prostokątny
                // stworzyc tablice dla 8 zmiennych technicznych                
                double[] techniczne = new double[9]; 
                /*
                krótka rozpiska
                techniczne[0]=Amplituda;
                techniczne[1]=Okres;
                techniczne[2]=J1;
                techniczne[3]=J2;
                techniczne[4]=N1;
                techniczne[5]=N2;
                techniczne[6]=k;
                techniczne[7]=b1;
                techniczne[8]=b2;
                */

                //Pobieranie danych
		try
                {
                    //odczyt wartości amplitudy // zastosowałem wartość bezwzględną by zabezpieczyć przed ujemnymi wartościami
                    techniczne[0] = Double.parseDouble(tAmplituda.getText());
                    techniczne[0]=abs(techniczne[0]);
                    //odczyt wartości okresu syganłu
                    techniczne[1] = abs(Double.parseDouble(tokres.getText()));
                    //odczyt wartości J1
                    techniczne[2] = abs(Double.parseDouble(J1t.getText()));
                    //odczyt wartości J2
                    techniczne[3] = abs(Double.parseDouble(J2t.getText()));
                    //odczyt wartości N1
                    techniczne[4] = abs(Double.parseDouble(N1t.getText()));
                    //odczyt wartości N2
                    techniczne[5] = abs(Double.parseDouble(N2t.getText()));
                    //odczyt wartości k
                    techniczne[6] = abs(Double.parseDouble(kt.getText()));
                    //odczyt wartości b1
                    techniczne[7] = abs(Double.parseDouble(b1t.getText()));
                    //odczyt wartości b2
                    techniczne[8] = abs(Double.parseDouble(b2t.getText()));
                    
                    //odczyt wartości czasu symulacji
                    czas = abs(Double.parseDouble(tCzasSymulacji.getText()));
                    //odczyt wartości czasu trwania kroku
                    krok = abs(Double.parseDouble(tCzasKroku.getText()));
                    //wybór metody symulacji, dla eulera 1 dla RK 2
                    if(radioEuler.isSelected()) {metoda = 1;}
                    else metoda = 2;
                    
                    //Wybór sygnału pobudzenia
                    if(radioImpuls.isSelected())
                    {
                        rodzaj_sygnalu=1;
                    }else if(radioSkok.isSelected())
                    {
                        rodzaj_sygnalu=2;
                    }else if(radioHarmoniczny.isSelected())
                    {
                        rodzaj_sygnalu=5;
                    }else if(radioProstokat.isSelected())
                    {
                        rodzaj_sygnalu=3;
                    }else if(radioTrojkat.isSelected())
                    {
                        rodzaj_sygnalu=4;
                    }                 			
                }catch(NumberFormatException excep)
                {		
                    JOptionPane.showMessageDialog(Pierwsze.this, "Please Enter the Right Info", "Error", JOptionPane.ERROR_MESSAGE);
                    wszystkoOK=false;
                    
		}
                    if(wszystkoOK)
                    {      
                        Matematyka licz = new Matematyka(); // tworzymy obiekt klasy matematyka
                 
                        //Definiowanie Wykresu
                        wykresPobudzenie = new XYSeries("Pobudzenie");
                        wykresKat1 = new XYSeries("Kąt1");
                        wykresKat2 = new XYSeries("Kąt2");
                        //wszystkie dane na jednym wspólnym wykresie
                        XYSeriesCollection zbiorWykresow = new XYSeriesCollection();
                        //dodawanie poszczególnych wykresów na jeden wykres
                        zbiorWykresow.addSeries(wykresPobudzenie);
                        zbiorWykresow.addSeries(wykresKat1);
                        zbiorWykresow.addSeries(wykresKat2);
                        JFreeChart FCWykres = ChartFactory.createXYLineChart("Wykresy", "czas", "Wartość", zbiorWykresow, PlotOrientation.VERTICAL, true, true, false);
                        ChartPanel CPWykres = new ChartPanel(FCWykres);      
                        //Tworzenie panelu pod wykres i ustawianie jego parametrów
                        Wykres.setBackground(new java.awt.Color(153, 153, 153));
                        Wykres.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 1, 0)));
                        Wykres.setLayout(new java.awt.BorderLayout());
                        // bez tego nie działa
                        Wykres.getAccessibleContext().setAccessibleName("");
        
                        Wykres.removeAll();
                        Wykres.add(CPWykres);
                        Wykres.validate();
                 
                        for (double i = 0; i <= czas; i += krok) //głowna pętla imitująca czas, i to sekundy
                        {
                            pobudz = licz.Sygnal(i, rodzaj_sygnalu, krok, techniczne); // wartość pobudzenia w danej chwili
            
                            if (metoda == 1) // wybór metody symulacji
                            {
                                if (i != 0) // potrzebujemy warunków początkowych więc nie możemy zacząć od zera.
                                {
                                    double poch_kat1_akt = poch_kat1; // przechowujemy wartość pochodnej kąta pierwszego, bo będziemy pracować na zmiennej poch_kat1
                                    poch_kat1 = licz.e_2_pochodna(pobudz, kat1, poch_kat1, krok, techniczne); // oblicza pierwszą pochodną na podstawie drugiej pochodnej za pomocą metody 
                                    kat1 = licz.E_kat1(kat1, poch_kat1_akt, krok);
                                    kat2 = licz.E_kat2(kat1, techniczne);
                                } 
                            }else if(metoda == 2)
                            {
                                if (i != 0)
                                {
                                    poch_kat1 = licz.RK_2_pochodna(pobudz, kat1, poch_kat1, i, krok, rodzaj_sygnalu, techniczne); // oblicza pierwszą pochodną na podstawie drugiej pochodnej za pomocą metody RK
                                    kat1 = licz.RK_kat1(kat1, krok);
                                    kat2 = licz.E_kat2(kat1, techniczne); //bo tak samo się zamienia kat1 na kat2
                                }
                            }
                            
                            if(checkPobudzenie.isSelected()) wykresPobudzenie.add(i,pobudz);
                            if(checkWalec1.isSelected()) wykresKat1.add(i,kat1);  
                            if(checWalec2.isSelected()) wykresKat2.add(i,kat2);
                            
                            
                               
      
			}//koniec głównej pętli imitującej czas			
                    }//koniec warunku poprawnosci dannych
            }// koniec getSource	
	}//koniec actionPerformed
		
    }//koniec sluchajStart
   
}//koniec klasy pierwsze