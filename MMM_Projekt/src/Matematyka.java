/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Marcin
 */
public class Matematyka 
{
    //definicja zmiennych, nazwy adekwatne do rysunku, może później zrobię je prywantne i dorobię metody by wyglądało to profesionalniej.
    public double q1,q2,q3,q4,k1,k2,k3,k4;
    /*
                Rozpiska
                techniczne[0]=Amplituda;
                techniczne[1]=Okres;
                techniczne[2]=J1;
                techniczne[3]=J2;
                techniczne[4]=N1;
                techniczne[5]=N2;
                techniczne[6]=k;
                techniczne[7]=b1;
                techniczne[8]=b2;
    */
            
            

    
    double Sygnal(double czas, int wybor, double krok, double[] techniczne) //Generowanie Pobudzeń
    {
        switch(wybor)
        {
            case 1: //impuls -> prawdziwy impuls trwa nieskończenie krótko, ale takiego program nie wyłapie, więc tu trwa jeden krok
            {
                if (czas <= krok) return 1/krok;
                else return 0;
                
            }
            case 2: // skok
            {
                if (czas > 0) return techniczne[0]; //amplituda
                else return 0;
            }
            case 3: // sygnal prostokątny #filtr
            {
                if(Math.sin(2*Math.PI*czas/techniczne[1])>0) return techniczne[0];
                else return -techniczne[0];
            }
            case 4: // Sygnał trójkątny, najtrudniejszy
            {
                if((Math.sin(2*Math.PI*czas/techniczne[1])>0) && (Math.cos(2*Math.PI*czas/techniczne[1])>0))
                {
                   czas = czas % techniczne[1];
                   return 4*techniczne[0]*czas/techniczne[1];
                 }
                 else if((Math.cos(2*Math.PI*czas/techniczne[1])<0))
                 {
                   czas = czas % techniczne[1];
                   return -4*techniczne[0]*czas/techniczne[1] + 2*techniczne[0];
                 }
           
                 else if((Math.sin(2*Math.PI*czas/techniczne[1])<0) && (Math.cos(2*Math.PI*czas/techniczne[1])>0))
                 {
                     czas = czas % techniczne[1];
                     return 4*techniczne[0]*czas/techniczne[1] - 4*techniczne[0];
                 }
            }
            case 5: // Harmoniczny
            {
                 return (techniczne[0]*Math.sin(2*Math.PI*czas/techniczne[1]));
            }
            default: return 0;
        }
        
    }//koniec sygnalu
    
    
    
    public double nachylenie(double pobudzenie, double aktualny_kat1, double aktualna_poch_kat1, double[] techniczne) // Obliczamy nachylenie, lub jak kto woli wartość drugiej pochodnej kąta pierwszego.
    {
        //Długaśne ale takie już być musi
        //return (techniczne[5]*techniczne[5])/(techniczne[5]*techniczne[5]*techniczne[2]+techniczne[4]*techniczne[4]*techniczne[3])*(pobudzenie - aktualna_poch_kat1*(techniczne[5]*techniczne[5]*techniczne[7] + techniczne[4]*techniczne[4]*techniczne[8])/(techniczne[5]*techniczne[5])-((techniczne[4]*techniczne[4])/(techniczne[5]*techniczne[5])*techniczne[6]*aktualny_kat1));
        //                                                                                                                                                                         ((N2*N2)/(N2*N2*J1+N1*N1*J2))*(pobudzenie-aktualna_poch_kat1*((N2*N2*b1+N1*N1*b2)/(N2*N2))-(aktualny_kat1*k)*((N1*N1)/(N2*N2)));
        return  ((techniczne[5]*techniczne[5])/(techniczne[5]*techniczne[5]*techniczne[2]+techniczne[4]*techniczne[4]*techniczne[3]))*(pobudzenie-aktualna_poch_kat1*((techniczne[5]*techniczne[5]*techniczne[7]+techniczne[4]*techniczne[4]*techniczne[8])/(techniczne[5]*techniczne[5]))-(aktualny_kat1*techniczne[6])*((techniczne[4]*techniczne[4])/(techniczne[5]*techniczne[5])));
    }
    
    // Liczenie Eulerem wartości pierwszej pochodnej kąta pierwszego, czyli bierzemy aktualną wartość pierwszej pochodnej kąta pierwszego
    // i dodając do niej wartość drugiej pochodnej kąta pierwszego przemnożonego przez krok uzyskujemy wartość pierwszej pochodnej kąta 
    //pierwszego w chwili następnej
    public double e_2_pochodna(double pobudzenie, double aktualny_kat1, double aktualna_poch_kat1,  double krok, double[] techniczne)
    {
        return aktualna_poch_kat1 + nachylenie(pobudzenie, aktualny_kat1, aktualna_poch_kat1, techniczne)*krok;
    }
    
    // Liczeniem Eulerem wartości kąta pierwszego dla danej chwili czasu, to samo co wyżej tyle że dla pierwszej pochodnej
    double E_kat1(double aktualny_kat1, double aktualna_poch_kat1, double krok){
        return aktualny_kat1 + aktualna_poch_kat1*krok;
    } 
    
    // prosta zależność wynikająca z ilości zębów w przekładniach N1 i N2
    double E_kat2(double kat1, double[] techniczne){
        return -kat1*(techniczne[4]/techniczne[5]);
    } 
    
    double RK_2_pochodna(  double pobudz, double kat1, double poch_kat1, double czas, double krok, int wybor, double[] techniczne)
    {
        //k - poch2Euler, q - funkcja 
        //double Sygnal(double czas, int wybor, double krok)
        double pobudz1 = Sygnal(czas + 0.5 * krok, wybor, krok, techniczne);//// bo potrzebujemy wartość pobudzenia które będzie za pół kroku
        double pobudz2 = Sygnal(czas + krok, wybor, krok, techniczne);// bo potrzebujemy wartość pobudzenia które będzie w następnym kroku
        
        k1 = krok*poch_kat1; // dla wartości kąta pierwszego
        q1 = krok*nachylenie( pobudz, kat1, poch_kat1, techniczne); // dla wartości pochodnej kąta pierwszego
        
        k2 = krok*(poch_kat1 + q1*0.5);
        q2 = krok*nachylenie(pobudz1, kat1 + k1*0.5, poch_kat1 + q1*0.5, techniczne);
        
        k3 = krok*(poch_kat1 + q2*0.5);
        q3 = krok*nachylenie(pobudz1, kat1 + k2*0.5, poch_kat1 + q2*0.5, techniczne);
        
        k4 = krok*(poch_kat1 + q3);
        q4 = krok*nachylenie(pobudz2, kat1 + k3, poch_kat1 + q3, techniczne);
        
     
       
        return   poch_kat1 + (q1+2.0*q2+2.0*q3+q4)/6.0; //Obliczyliśmy następną wartość pochodnej kąta pierwszego
    }
    
    double RK_kat1(double kat1, double krok) // znając współczyniki k i aktualny kat1 możemy poznać kolejny kat1
    {
       
        return kat1 + (k1+2.0*k2+2.0*k3+k4)/6.0;
    }
        
   
    
    
    
}//koniec Klasy Matematyka
