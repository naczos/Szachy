#include <iostream>
#include <string>
#include <cstdlib>
#include <string.h>
#include <vector>
#include <algorithm>
using namespace std;

class Punkt
{
	private:
		string nazwa;
		int WspX, WspY;
	
	public:
		int odleglosKwadrat;
		void rozdzialDanejWejsciowejNaSkladowe(string danaWejsciowa);
		void wypiszDane();
		Punkt();
		void kopiujDane(Punkt zrodlo);
		Punkt(string);
		
		bool operator < (const Punkt &x)const
	    {
	        return this->odleglosKwadrat < x.odleglosKwadrat;
	    }
	
//	Punkt (string danaWejsciowa)
//	{
//		rozdzialDanejWejsciowejNaSkladowe(danaWejsciowa);
//	}
//	
	
};

void Punkt::kopiujDane(Punkt zrodlo)
{
	this->nazwa = zrodlo.nazwa;
	this->odleglosKwadrat = zrodlo.odleglosKwadrat;
	this->WspX = zrodlo.WspX;
	this->WspY = zrodlo.WspY;
}

//dane w DanychWejsciowych s� w formacie Nazwa wspX wspY, a wi�c trzeba je wyciagnac 
//przez rozdzielenie spacjami
Punkt::Punkt(string danaWejsciowa)
{
	this->rozdzialDanejWejsciowejNaSkladowe(danaWejsciowa);
}

Punkt::Punkt()
{
	cout<<"dupa Cycki"<<endl;
}

void Punkt::rozdzialDanejWejsciowejNaSkladowe(string daneWejsciowe)
{
	int daneWejscioweIndexSpacji = daneWejsciowe.find(" ",0);	
	
	this->nazwa = daneWejsciowe.substr(0,daneWejscioweIndexSpacji);
	this->nazwa[daneWejscioweIndexSpacji]= '\0';
	daneWejsciowe.erase(0,daneWejscioweIndexSpacji+1);
	
	string xSlownie, ySlownie;
	
	daneWejscioweIndexSpacji = daneWejsciowe.find(" ", 0);
	xSlownie = daneWejsciowe.substr(0, daneWejscioweIndexSpacji);
	daneWejsciowe.erase(0,daneWejscioweIndexSpacji+1);
	
	daneWejscioweIndexSpacji = daneWejsciowe.find(" ", 0);
	ySlownie = daneWejsciowe.substr(0, daneWejscioweIndexSpacji);
	
	
	this->WspX = atoi(xSlownie.c_str());
	this->WspY = atoi(ySlownie.c_str());
	
	this->odleglosKwadrat = this->WspX * this->WspX + this->WspY * this->WspY;
	
}


void Punkt::wypiszDane()
{
	cout<<"Oto klasa: "<<this->nazwa<<" "<<this->WspX<<" "<<this->WspY<<" "<<this->odleglosKwadrat<<endl;
}

int partition(long int tablica[], int p, int r, int buforX[], int buforY[], char buforNazw[][11]) // dzielimy tablice na dwie czesci, w pierwszej wszystkie liczby sa mniejsze badz rowne x, w drugiej wieksze lub rowne od x
{
	long int x = tablica[p]; // obieramy x
	int i = p, j = r; // i, j - indeksy w tabeli
	long int w;
	int bufX, bufY;
	string bufNazwa, bufNazwa1;
	
	while (true) // petla nieskonczona - wychodzimy z niej tylko przez return j
		{
		while (tablica[j] > x) // dopoki elementy sa wieksze od x
			j--;
		while (tablica[i] < x) // dopoki elementy sa mniejsze od x
			i++;
		if (i < j) // zamieniamy miejscami gdy i < j
		{
			w = tablica[i];
			bufX=buforX[i];
			bufY=buforY[i];
			bufNazwa=buforNazw[i];
			
			tablica[i] = tablica[j];
			buforX[i] = buforX[j];
			buforY[i] = buforY[j];
			//buforNazw[i] = buforNazw[j]
			bufNazwa1=buforNazw[j];
			int dlugoscNazwy = bufNazwa1.copy(buforNazw[i], bufNazwa1.length(),0);
			buforNazw[i][dlugoscNazwy]= '\0';
			
			tablica[j] = w;
			buforX[j]=bufX;
			buforY[j]=bufY;
			dlugoscNazwy = bufNazwa.copy(buforNazw[j], bufNazwa.length(),0);
			buforNazw[j][dlugoscNazwy]= '\0';
			//buforNazw[j]=bufNazwa;
			
			i++;
			j--;
		}
		else // gdy i >= j zwracamy j jako punkt podzialu tablicy
		return j;
	}
}

void quicksort(long int tablica[], int p, int r, int buforX[], int buforY[], char buforNazw[][11]) // sortowanie szybkie
{
	int q;
	if (p < r)
	{  
		q = partition(tablica,p,r, buforX, buforY, buforNazw); // dzielimy tablice na dwie czesci; q oznacza punkt podzialu
		quicksort(tablica, p, q, buforX, buforY, buforNazw); // wywolujemy rekurencyjnie quicksort dla pierwszej czesci tablicy
		quicksort(tablica, q+1, r, buforX, buforY, buforNazw); // wywolujemy rekurencyjnie quicksort dla drugiej czesci tablicy
	}
}


void obliczanieOdleglosci(long int buforOdleglosc[], int buforX[], int buforY[], int n)
{
	for(int i=0; i<n; i++)
	{
		buforOdleglosc[i]=buforX[i]*buforX[i]+buforY[i]*buforY[i];
	}
}

void rozdzielanieSpacja(string daneWejsciowe, char buforNazw[][11], char xSlownie[], char ySlownie[], int licznikPunktow)
{
	int daneWejscioweIndexSpacji = daneWejsciowe.find(" ",0);
	int dlugoscWsp;		
	int dlugoscNazwy;
	
	dlugoscNazwy = daneWejsciowe.copy(buforNazw[licznikPunktow], daneWejscioweIndexSpacji,0);
	daneWejsciowe.erase(0,daneWejscioweIndexSpacji+1);
	buforNazw[licznikPunktow][dlugoscNazwy]= '\0';
		
	daneWejscioweIndexSpacji = daneWejsciowe.find(" ",0);

	dlugoscWsp = daneWejsciowe.copy(xSlownie, daneWejscioweIndexSpacji,0);
	daneWejsciowe.erase(0,daneWejscioweIndexSpacji+1);
	xSlownie[dlugoscWsp]= '\0';
	
	dlugoscWsp = daneWejsciowe.copy(ySlownie, daneWejsciowe.length(),0);
	ySlownie[dlugoscWsp]= '\0';
	
}

void wczytywanieDanych(char buforNazw[][11], int buforX[], int buforY[], int n)
{
	string daneWejsciowe;
	char ySlownie[6];
	char xSlownie[6];
	int x, y;	
	//Punkt * punkty = new Punkt[n];
	
	
	for(int licznikPunktow=0; licznikPunktow<n; licznikPunktow++)
	{
			
		getline(cin, daneWejsciowe);
	
		rozdzielanieSpacja(daneWejsciowe, buforNazw, xSlownie, ySlownie, licznikPunktow);
		
		x = atoi(xSlownie);
		y = atoi(ySlownie);
		
		buforX[licznikPunktow] = x;	
		buforY[licznikPunktow] = y;
		
		//cout<<"Oto wspX,wspY: "<<buforX[licznikPunktow]<<","<<buforY[licznikPunktow]<<"a oto nazwa: "<<buforNazw[licznikPunktow]<<licznikPunktow<<endl;
		
		
	
	}
}


void wypisywanieDanych(char buforNazw[][11], int buforX[], int buforY[], int n)
{
	for(int i=0; i<n; i++)
	{
		cout<<buforNazw[i]<<" "<<buforX[i]<<" "<<buforY[i]<<endl;
	}
}

int partitionVector(vector<Punkt>& punkty, int p, int r)
{
	int x = punkty[p].odleglosKwadrat; // obieramy x
	int i = p, j = r; // i, j - indeksy w tabeli
	Punkt bufPunkt();

	
	while (true) // petla nieskonczona - wychodzimy z niej tylko przez return j
	{
		while (punkty[j].odleglosKwadrat > x) // dopoki elementy sa wieksze od x
			j--;
		while (punkty[i].odleglosKwadrat < x) // dopoki elementy sa mniejsze od x
			i++;
		if (i < j) // zamieniamy miejscami gdy i < j
		{
			//cout<<typeof(bufPunkt);
		//	bufPunkt.kopiujDane(punkty[i]);
			
		//	punkty[i] = punkty[j];
			
		//	punkty[j] = bufPunkt;
			
			i++;
			j--;
		}
		else // gdy i >= j zwracamy j jako punkt podzialu tablicy
		return j;
	}
}

void quicksortVector(vector<Punkt>& punkty, int p, int r) // sortowanie szybkie
{
	int q;
	if (p < r)
	{  
		q = partitionVector(punkty,p,r); // dzielimy tablice na dwie czesci; q oznacza punkt podzialu
		quicksortVector(punkty, p, q); // wywolujemy rekurencyjnie quicksort dla pierwszej czesci tablicy
		quicksortVector(punkty, q+1, r); // wywolujemy rekurencyjnie quicksort dla drugiej czesci tablicy
	}
}


int main()
{
	int t, n;
	char buforNazw[1000][11];
	int buforX[1000];
	int buforY[1000];
	long int buforOdleglosc[1000];
	
	
	cin>>t;
	for(int licznikProb=0; licznikProb<t; licznikProb++)
	{
		cin>>n;
		cin.ignore();
		//Punkt *punkty = new Punkt[n];
		vector<Punkt> punkty;

		
		for(int licznikPunktow=0; licznikPunktow<n; licznikPunktow++)
		{
			string daneWejsciowe;
			getline(cin, daneWejsciowe);
			Punkt nowyPunkt(daneWejsciowe);
			//nowyPunkt->rozdzialDanejWejsciowejNaSkladowe(daneWejsciowe);
		//	nowyPunkt->wypiszDane();
			
			//punkty[licznikPunktow]->rozdzialDanejWejsciowejNaSkladowe(danaWejsciowa);
			punkty.push_back(nowyPunkt);
		
		}
		
		cout<<endl;
		
		//quicksortVector(punkty,0,n-1);
		sort (punkty.begin(), punkty.begin()+n-1); 
		
		for(int licznikPunktow=0; licznikPunktow<n; licznikPunktow++)
		{
			punkty[licznikPunktow].wypiszDane();
		}	
		
//		wczytywanieDanych(buforNazw, buforX, buforY, n);
//		obliczanieOdleglosci(buforOdleglosc, buforX, buforY, n);
//		quicksort(buforOdleglosc,0,n-1, buforX, buforY, buforNazw); // wywolanie funkcji sortujacej
//		wypisywanieDanych(buforNazw, buforX, buforY, n);
		cout<<endl;
	}
	
	return 0;
}
