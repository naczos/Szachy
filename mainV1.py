import numpy as np
from termcolor import colored, cprint
import os
#plansza = np.zeros(64)

podpis= [' a',' b',' c',' d',' e',' f',' g',' h']
osiem= ['xx','  ','xx','  ','xx','  ','xx','  ','8']
siedem= ['  ','xx','  ','xx','  ','xx','  ','xx','7']
szesc= ['xx','  ','xx','  ','xx','  ','xx','  ','6']
piec= ['  ','xx','  ','xx','  ','xx','  ','xx','5']
cztery= ['xx','  ','xx','  ','xx','  ','xx','  ','4']
trzy= ['  ','xx','  ','xx','  ','xx','  ','xx','3']
dwa= ['xx','  ','xx','  ','xx','  ','xx','  ','2']
jeden= ['  ','xx','  ','xx','  ','xx','  ','xx','1']

podpis1= [' a',' b',' c',' d',' e',' f',' g',' h']
osiem1= ['xx','  ','xx','  ','xx','  ','xx','  ','8']
siedem1= ['  ','xx','  ','xx','  ','xx','  ','xx','7']
szesc1= ['xx','  ','xx','  ','xx','  ','xx','  ','6']
piec1= ['  ','xx','  ','xx','  ','xx','  ','xx','5']
cztery1= ['xx','  ','xx','  ','xx','  ','xx','  ','4']
trzy1= ['  ','xx','  ','xx','  ','xx','  ','xx','3']
dwa1= ['xx','  ','xx','  ','xx','  ','xx','  ','2']
jeden1= ['  ','xx','  ','xx','  ','xx','  ','xx','1']

plansza=[jeden, dwa,trzy,cztery,piec,szesc,siedem,osiem]

plansza1=[jeden1, dwa1,trzy1,cztery1,piec1,szesc1,siedem1,osiem1]




def rysuj():
    #print("\033[H\033[J")
    print('\n'*10)
    biala= True
    #print(podpis)
    for i in podpis:
        cprint(' ', 'cyan', end='')
        cprint(i, 'cyan', end='')
        cprint(' ', 'cyan', end='')
    cprint('', 'blue', end='\n')
    #cprint(plansza[7], 'grey', 'on_white')
    for j in range(8):
        for i in plansza[7-j]:
            if biala:
                cprint(' ', 'cyan', 'on_grey', end='')
                cprint(i, 'cyan','on_grey', end='')
                cprint(' ', 'cyan', 'on_grey', end='')
                biala = False
            else:
                cprint(' ', 'cyan', 'on_green', end='')
                cprint(i, 'cyan', 'on_green', end='')
                cprint(' ', 'cyan', 'on_green', end='')
                biala = True
        cprint('', 'blue', end='\n')
    #print(podpis)
    for i in podpis:
        cprint(' ', 'cyan', end='')
        cprint(i, 'cyan', end='')
        cprint(' ', 'cyan', end='')
    #cprint(podpis, 'cyan', 'on_blue', end='')
    print()



class pionek(object):
    def __init__(self, wiersz, kolumna, kolor, id):
        self.wiersz = wiersz
        self.kolumna = kolumna
        self.kolor = kolor
        self.zbity = False
        self.pierwszy = True
        #self.nazwa = ''

    def ruch(self, wierszCel, kolumnaCel):
        ruchy = self.dostepneRuchy(False)
        #print("oto dostepne ruchy :)")
        wykonaRuch = False
        for ruch in ruchy:
            wierszMozliwosc, kolumnaMozliwosc, bicie, pioneczek = ruch
            if wierszMozliwosc == wierszCel and kolumnaMozliwosc == kolumnaCel:
                wykonaRuch = True
            #print(ruch)

        if wykonaRuch:
            #dziwny, ale specjany warunek dla roszady w lewo
            if kolumnaCel == -6:
                plansza[self.wiersz][2] = self.nazwa
                plansza[self.wiersz][self.kolumna] = plansza1[self.wiersz][self.kolumna]
                self.kolumna = 2

                plansza[pioneczek.wiersz][3] = pioneczek.nazwa
                plansza[pioneczek.wiersz][pioneczek.kolumna] = plansza1[pioneczek.wiersz][pioneczek.kolumna]
                pioneczek.kolumna = 3
            else:
                    # dziwny, ale specjany warunek dla roszady w prawo
                if kolumnaCel == -4:
                    plansza[self.wiersz][6] = self.nazwa
                    plansza[self.wiersz][self.kolumna] = plansza1[self.wiersz][self.kolumna]
                    self.kolumna = 6

                    plansza[pioneczek.wiersz][5] = pioneczek.nazwa
                    plansza[pioneczek.wiersz][pioneczek.kolumna] = plansza1[pioneczek.wiersz][pioneczek.kolumna]
                    pioneczek.kolumna = 5

                else:

                    plansza[wierszCel][kolumnaCel]=self.nazwa
                    plansza[self.wiersz][self.kolumna]=plansza1[self.wiersz][self.kolumna]
                    self.pierwszy = False
                    if bicie:
                        pioneczek.zbity=True
                    self.wiersz=wierszCel
                    self.kolumna = kolumnaCel

                    #Sprawdzanie czy nastapi zamiana piona na inna figure
                    if self.kolor=='biale' and self.wiersz==7:
                        if self.nazwa == 'p0' or self.nazwa == 'p1' or self.nazwa == 'p2' or self.nazwa == 'p3' or self.nazwa == 'p4' or self.nazwa == 'p5' or self.nazwa == 'p6' or self.nazwa == 'p7':
                            self.zbity= True
                            self.nowaBierka()

                    if self.kolor == 'czarne' and self.wiersz == 0:
                        if self.nazwa == 'P0' or self.nazwa == 'P1' or self.nazwa == 'P2' or self.nazwa == 'P3' or self.nazwa == 'P4' or self.nazwa == 'P5' or self.nazwa == 'P6' or self.nazwa == 'P7':
                            self.zbity = True
                            self.nowaBierka()
        # if plansza[wierszCel][kolumnaCel]=='x' or plansza[wierszCel][kolumnaCel]==' ':
        #     plansza[wierszCel][kolumnaCel]='p'
        #     plansza[self.wiersz][self.kolumna]=plansza1[self.wiersz][self.kolumna]

        return wykonaRuch

    def wolnePole(self, wierszCel, kolumnaCel):
        wolne=True
        bicie = False
        pionekDoZbicia = domyslnyPionek
        pole= plansza[wierszCel][kolumnaCel]
        if pole == '  ' or pole == "xx":
            wolne=True
        else:
            wolne = False
            nazwaPionka, pioneczek = ktoryPionek(wierszCel,kolumnaCel)
            #print("nacelowalem: ",pioneczek.nazwa)
            if pioneczek.kolor == self.kolor:
                bicie=False
            else:
                bicie = True
                pionekDoZbicia = pioneczek

        return wolne, bicie, pionekDoZbicia

    def nowaBierka(self):
        while True:
            print("jaką chcesz bierkę?")
            print("Dama: 1")
            print("Kon: 2")
            print("Skoczek: 3")
            print("Wierza: 4")
            nazwaNowej = int(input())
            if nazwaNowej == 1:
                figury.append(dama(self.wiersz, self.kolumna, self.kolor, 1))
                break
            else:
                if nazwaNowej == 2:
                    figury.append(kon(self.wiersz, self.kolumna, self.kolor, 1))
                    break
                else:
                    if nazwaNowej == 3:
                        figury.append(skoczek(self.wiersz, self.kolumna, self.kolor, 1))
                        break
                    else:
                        if nazwaNowej == 4:
                            figury.append(wierza(self.wiersz, self.kolumna, self.kolor, 1))
                            break
                        else:
                            print("zly klawisz.")





#tworzenie randomello pionka
domyslnyPionek = pionek(100,100,'szary', 500)

class pion(pionek):
    def __init__(self, wiersz, kolumna, kolor, id):
        super().__init__(wiersz, kolumna, kolor, id)
        if self.kolor == 'biale':
            self.nazwa = 'p'+str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

        if self.kolor == 'czarne':
            self.nazwa = 'P' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa



    def dostepneRuchy(self, wyswietlanie):
        ruchy = []
        # pion ma dostepne 4 ruchy, na prost, na prost x2, i po skosie
        #print("teraz jestem: ", self.kolor)
        if self.kolor=='biale':
            wierszCel=self.wiersz+1
        else:
            wierszCel = self.wiersz - 1

        kolumnaCel = self.kolumna
        wolne, bicie, pioneczek = self.wolnePole(wierszCel,kolumnaCel)
        if wolne:
            ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
            ruchy.append(ruch)
        #powyzej sprawdzono czy pion moze sie poruszyc do przodu o jeden

        #print('moge sie ruszyc do: ', wierszCel, " ", kolumnaCel, " ", wolne)

        if self.pierwszy and ruchy.__len__()>0:
            if self.kolor == 'biale':
                wierszCel = self.wiersz + 2
            else:
                wierszCel = self.wiersz - 2

            kolumnaCel = self.kolumna
            pole = self.wolnePole(wierszCel, kolumnaCel)
            if pole:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
        # powyzej sprawdzono czy pion moze sie poruszyc do przodu o dwa


        if self.kolor == 'biale':
            wierszCel = self.wiersz + 1
        else:
            wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna-1

        if kolumnaCel>=0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if (not wolne) and bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        if self.kolor == 'biale':
            wierszCel = self.wiersz + 1
        else:
            wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna+1

        if kolumnaCel<7:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if (not wolne) and bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
        if wyswietlanie:
            print("DOSTEPNE RUCHY")
            for ruch in ruchy:
                wiersz, kolumna, bicie, pionek = ruch
                wierszSzachowo = wiersz + 1
                kolumnaSzachowo = chr(97+kolumna)
                print(kolumnaSzachowo,wierszSzachowo)

        return ruchy



class wierza(pionek):
    def __init__(self, wiersz, kolumna, kolor, id):
        super().__init__(wiersz, kolumna, kolor, id)
        if self.kolor == 'biale':
            self.nazwa = 'w'+str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

        if self.kolor == 'czarne':
            self.nazwa = 'W' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

    def dostepneRuchy(self, wyswietlanie):
        ruchy = []
        # wierza ma dostep do 4 rodzajow ruchow, na wprost do przodu do tyłu na lewo i prawo
        # print("teraz jestem: ", self.kolor)

        #na wprost w przod
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna
            if wierszCel<8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break


        # na wprost w tyl
        for i in range(7):
            wierszCel = self.wiersz - i - 1
            kolumnaCel = self.kolumna
            if wierszCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na lewo
        for i in range(7):
            wierszCel = self.wiersz
            kolumnaCel = self.kolumna - i -1
            if kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na prawo
        for i in range(7):
            wierszCel = self.wiersz
            kolumnaCel = self.kolumna + i +1
            if kolumnaCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break
        if wyswietlanie:
            print("DOSTEPNE RUCHY")
            for ruch in ruchy:
                wiersz, kolumna, bicie, pionek = ruch
                wierszSzachowo = wiersz + 1
                kolumnaSzachowo = chr(97 + kolumna)
                print(kolumnaSzachowo, wierszSzachowo)

        return ruchy



class kon(pionek):
    def __init__(self, wiersz, kolumna, kolor, id):
        super().__init__(wiersz, kolumna, kolor, id)
        if self.kolor == 'biale':
            self.nazwa = 'h'+str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

        if self.kolor == 'czarne':
            self.nazwa = 'H' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

    def dostepneRuchy(self, wyswietlanie):
        ruchy = []
        # wierza ma dostep do 4 rodzajow ruchow, na wprost do przodu do tyłu na lewo i prawo
        # print("teraz jestem: ", self.kolor)


        # na wprost w przod

        wierszCel = self.wiersz + 1
        kolumnaCel = self.kolumna + 2
        if wierszCel < 8 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        # na wprost w tyl

        wierszCel = self.wiersz + 2
        kolumnaCel = self.kolumna + 1
        if wierszCel < 8 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz + 2
        kolumnaCel = self.kolumna - 1
        if wierszCel < 8 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz + 1
        kolumnaCel = self.kolumna - 2
        if wierszCel < 8 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz - 2
        kolumnaCel = self.kolumna + 1
        if wierszCel >= 0 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna + 2
        if wierszCel >= 0 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz - 2
        kolumnaCel = self.kolumna - 1
        if wierszCel >= 0 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna - 2
        if wierszCel >= 0 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
        if wyswietlanie:
            print("DOSTEPNE RUCHY")
            for ruch in ruchy:
                wiersz, kolumna, bicie, pionek = ruch
                wierszSzachowo = wiersz + 1
                kolumnaSzachowo = chr(97 + kolumna)
                print(kolumnaSzachowo, wierszSzachowo)

        return ruchy


class skoczek(pionek):
    def __init__(self, wiersz, kolumna, kolor, id):
        super().__init__(wiersz, kolumna, kolor, id)
        if self.kolor == 'biale':
            self.nazwa = 's'+str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

        if self.kolor == 'czarne':
            self.nazwa = 'S' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

    def dostepneRuchy(self, wyswietlanie):
        ruchy = []
        # wierza ma dostep do 4 rodzajow ruchow, na wprost do przodu do tyłu na lewo i prawo
        # print("teraz jestem: ", self.kolor)

        #na wprost w 45 stopni
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna + i + 1
            if wierszCel<8 and kolumnaCel<8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break


        # na wprost -45 stopni
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna - i - 1
            if wierszCel < 8 and kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # w tył 45 stopni
        for i in range(7):
            wierszCel = self.wiersz - i -1
            kolumnaCel = self.kolumna + i + 1
            if wierszCel >= 0 and kolumnaCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # w tył -45 stopni
        for i in range(7):
            wierszCel = self.wiersz - i - 1
            kolumnaCel = self.kolumna - i - 1
            if wierszCel >= 0 and kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break
        if wyswietlanie:
            print("DOSTEPNE RUCHY")
            for ruch in ruchy:
                wiersz, kolumna, bicie, pionek = ruch
                wierszSzachowo = wiersz + 1
                kolumnaSzachowo = chr(97 + kolumna)
                print(kolumnaSzachowo, wierszSzachowo)

        return ruchy

class dama(pionek):
    def __init__(self, wiersz, kolumna, kolor, id):
        super().__init__(wiersz, kolumna, kolor, id)
        if self.kolor == 'biale':
            self.nazwa = 'd'+str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

        if self.kolor == 'czarne':
            self.nazwa = 'D' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

    def dostepneRuchy(self, wyswietlanie):
        ruchy = []
        # wierza ma dostep do 4 rodzajow ruchow, na wprost do przodu do tyłu na lewo i prawo
        # print("teraz jestem: ", self.kolor)


        # na wprost w przod
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna
            if wierszCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na wprost w tyl
        for i in range(7):
            wierszCel = self.wiersz - i - 1
            kolumnaCel = self.kolumna
            if wierszCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na lewo
        for i in range(7):
            wierszCel = self.wiersz
            kolumnaCel = self.kolumna - i - 1
            if kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na prawo
        for i in range(7):
            wierszCel = self.wiersz
            kolumnaCel = self.kolumna + i + 1
            if kolumnaCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        #na wprost w 45 stopni
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna + i + 1
            if wierszCel<8 and kolumnaCel<8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break


        # na wprost -45 stopni
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna - i - 1
            if wierszCel < 8 and kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # w tył 45 stopni
        for i in range(7):
            wierszCel = self.wiersz - i -1
            kolumnaCel = self.kolumna + i + 1
            if wierszCel >= 0 and kolumnaCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # w tył -45 stopni
        for i in range(7):
            wierszCel = self.wiersz - i - 1
            kolumnaCel = self.kolumna - i - 1
            if wierszCel >= 0 and kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break
        if wyswietlanie:
            print("DOSTEPNE RUCHY")
            for ruch in ruchy:
                wiersz, kolumna, bicie, pionek = ruch
                wierszSzachowo = wiersz + 1
                kolumnaSzachowo = chr(97 + kolumna)
                print(kolumnaSzachowo, wierszSzachowo)

        return ruchy

class krol(pionek):
    def __init__(self, wiersz, kolumna, kolor, id):
        super().__init__(wiersz, kolumna, kolor, id)
        if self.kolor == 'biale':
            self.nazwa = 'k'+str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

        if self.kolor == 'czarne':
            self.nazwa = 'K' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

    def dostepneRuchy(self, wyswietlanie):
        ruchy = []
        # wierza ma dostep do 4 rodzajow ruchow, na wprost do przodu do tyłu na lewo i prawo
        # print("teraz jestem: ", self.kolor)


        # na wprost w przod

        wierszCel = self.wiersz + 1
        kolumnaCel = self.kolumna
        if wierszCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)


        # na wprost w tyl

        wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna
        if wierszCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        # na lewo
        wierszCel = self.wiersz
        kolumnaCel = self.kolumna - 1
        if kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        # na prawo
        wierszCel = self.wiersz
        kolumnaCel = self.kolumna + 1
        if kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)



        #na wprost w 45 stopni
        wierszCel = self.wiersz + 1
        kolumnaCel = self.kolumna + 1
        if wierszCel<8 and kolumnaCel<8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)




        # na wprost -45 stopni
        wierszCel = self.wiersz + 1
        kolumnaCel = self.kolumna - 1
        if wierszCel < 8 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)


    # w tył 45 stopni
        wierszCel = self.wiersz -1
        kolumnaCel = self.kolumna + 1
        if wierszCel >= 0 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)


        # w tył -45 stopni

        wierszCel = self.wiersz  - 1
        kolumnaCel = self.kolumna  - 1
        if wierszCel >= 0 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        #Roszada
        #Sprawdzić czy król nie jest szachowany i czy to jego pierwszy ruch
        szach = CzySzach(self.kolor, False) #drugi argument mowi o tym czy wyswietlac dane z funkcji
        if (not szach) and self.pierwszy:
           # print('nie ma szachu i pierwszy ruch')
            #sprawdzanie czy nie ma niczego pomiedzy krolem a wierza z lewej strony
            if ((plansza[self.wiersz][3] == 'xx' or  plansza[self.wiersz][3] == "  ") and
                (plansza[self.wiersz][2] == 'xx' or plansza[self.wiersz][2] == "  ") and
                (plansza[self.wiersz][1] == 'xx' or plansza[self.wiersz][1] == "  ")):
               # print('pomiedzy nic nie ma')
                nazwaWierza, wierza1 = ktoryPionek(self.wiersz, 0)
                #sprawdzanie czy tam stoi wierza i czy to jej pierwszy ruch
                if (self.kolor == 'biale' and nazwaWierza=='w0') or (self.kolor == 'czarne' and nazwaWierza=='W0') and wierza1.pierwszy:
                   # print('i tak tam jest wierza')
                    #Jesli jest mozliwa roszada to ruch mowi ze wiersz taki sam, kolumna ROWNA - 77 bicia brak, pioneczek to wierza do zamiany
                    ruch= [self.wiersz, -6, False, wierza1]
                    ruchy.append(ruch)

            # sprawdzanie czy nie ma niczego pomiedzy krolem a wierza z prawej strony
            if ((plansza[self.wiersz][5] == 'xx' or plansza[self.wiersz][5] == "  ") and
                    (plansza[self.wiersz][6] == 'xx' or plansza[self.wiersz][6] == "  ")):
                nazwaWierza, wierza1 = ktoryPionek(self.wiersz, 0)
                # sprawdzanie czy tam stoi wierza i czy to jej pierwszy ruch
                if (self.kolor == 'biale' and nazwaWierza == 'w0') or (
                        self.kolor == 'czarne' and nazwaWierza == 'W0') and wierza1.pierwszy:
                    # Jesli jest mozliwa roszada to ruch mowi ze wiersz taki sam, kolumna ROWNA - 77 bicia brak, pioneczek to wierza do zamiany
                    ruch = [self.wiersz, -4, False, wierza1]
                    ruchy.append(ruch)

        if wyswietlanie:
            print("DOSTEPNE RUCHY")
            for ruch in ruchy:
                wiersz, kolumna, bicie, pionek = ruch
                wierszSzachowo = wiersz + 1
                kolumnaSzachowo = chr(97 + kolumna)
                print(kolumnaSzachowo, wierszSzachowo)

        return ruchy



figury = []

for i in range(8):
    figury.append(pion(5,i,'biale',i))
    #figury.append(pion(6,i,'czarne',i))

figury.append(wierza(0,0,'biale',0))
figury.append(wierza(0,7,'biale',1))
#figury.append(wierza(7,0,'czarne',0))
figury.append(wierza(7,7,'czarne',1))
#figury.append(kon(0,1,'biale',0))
#figury.append(kon(0,6,'biale',1))
figury.append(kon(7,6,'czarne',1))
figury.append(kon(7,1,'czarne',0))
figury.append(skoczek(7,2,'czarne',0))
figury.append(skoczek(7,5,'czarne',1))
#figury.append(skoczek(0,5,'biale',1))
#figury.append(skoczek(0,2,'biale',0))
#figury.append(dama(0,3,'biale',0))
figury.append(dama(7,3,'czarne',0))
figury.append(krol(7,4,'czarne',0))
figury.append(krol(0,4,'biale',0))

#tu dowane testowe figury
#figury.append(krol(2,3,'czarne',0))
#figury.append(kon(4,3,'biale',2))
#figury.append(krol(2,2,'czarne',0))





def ktoryPionek(wiersz, kolumna):
    nazwa = ''
    wybranaFigura = domyslnyPionek
    for pioneczek in figury:
        if pioneczek.kolumna == kolumna and pioneczek.wiersz == wiersz and (not pioneczek.zbity):
            nazwa = pioneczek.nazwa
            wybranaFigura = pioneczek
            break


    return nazwa, wybranaFigura

def szachoweNaIndex(slowo):
    wiersz = 100
    kolumna = 100
    dobreDane = False
    ASCI = [ord(x) for x in slowo]
    #print('To jest ascii: ',ASCI)
    if ASCI.__len__() != 2:
        print("Złe dane")
        return wiersz, kolumna, dobreDane
    else:
        first = int(ASCI[0])  # pozniej zabiezpieczyc przed znakami polskimi
        second = int(ASCI[1])
        #print('a to juz liczby:', first, ' ',second)
        if first > 96 and first < 105 or first==93 or first==91:
            kolumna=first-97
        if first > 64 and first < 73:
            kolumna = first - 65

        if second > 48 and second < 57:
            wiersz = second - 49

    if wiersz <8 and kolumna < 8:
        dobreDane=True
    else:
        print("Złe dane")

    return wiersz, kolumna, dobreDane

def CzySzach(kolor, wyswietlanie):
    szach = False
    #lista pionkow ktore moga szachowac krola
    atakujacy = []

    #Wyszukaj gdzie jest król
    for pioneczek in figury:
        if (pioneczek.nazwa == 'k0' or pioneczek.nazwa == 'K0') and pioneczek.kolor == kolor:
           krol=pioneczek

    #pobranie wszystkich ruchow przeciwnikow
    for pioneczek in figury:
        if pioneczek.kolor != kolor and (not pioneczek.zbity) and (not(pioneczek.nazwa == 'k0' or pioneczek.nazwa =='K0')):
            if wyswietlanie:
                print("Sprawdzam ruch: ", pioneczek.nazwa)
            ruchy=pioneczek.dostepneRuchy(False)
            for ruch in ruchy:
                wiersz, kolumna, bicie, atakowanyPioneczek = ruch
                if atakowanyPioneczek == krol: #jesli ktorykolwiek z mozliwych do zaatakowania pionkow jest krolem wpisz atakujacego na liste atakujacych
                    atakujacy.append(pioneczek)
                    szach = True
    if wyswietlanie:
        print(atakujacy)
    return szach

def szachMat():
    print("ło kurwa ale tu bedzie do pisania")




def Ruszaj():
    kolor = 'biale'

    while True:

        szach=CzySzach(kolor, False) # drugi argument mowi o tym czy ma sie wyswietlac cos z funkcji
        print("jest szach?: ",szach)
        poprawnyRuch = False
        print("Ruszaja ",kolor)
        rysuj()
        slowo=input('Podaj Pole: ')
        wiersz, kolumna, dobreDane = szachoweNaIndex(slowo)
        if dobreDane:
            #print("wybrałeś taki wiersz i kolumne: ", wiersz,", ",kolumna)
            nazwaPionka, wybranyPionek = ktoryPionek(wiersz, kolumna)
            if nazwaPionka != '':
                if wybranyPionek.kolor == kolor:
                    print("wybrales: ", wybranyPionek.nazwa)
                    wybranyPionek.dostepneRuchy(True)
                    slowoCel = input('Podaj Pole: ')
                    wierszCel, kolumnaCel, dobreDaneCel = szachoweNaIndex(slowoCel)
                    if dobreDane:
                        poprawnyRuch = wybranyPionek.ruch(wierszCel, kolumnaCel)
                        if not poprawnyRuch:
                            print("wybrany pionek nie ma mozliwosci ruchu")
                else:
                    print("Nie możesz sterować pionkiem przeciwnika")
            else:
                print("Nie wybrales pionka.")


        if poprawnyRuch:
            if kolor == 'biale':
                kolor = 'czarne'
            else:
                kolor = 'biale'


#print(ktoryPionek(1,3))


Ruszaj()