import numpy as np
from termcolor import colored, cprint
import os




# Plansza na ktorej toczy sie gra
podpis = [' a', ' b', ' c', ' d', ' e', ' f', ' g', ' h']
osiem = ['xx', '  ', 'xx', '  ', 'xx', '  ', 'xx', '  ', '8']
siedem = ['  ', 'xx', '  ', 'xx', '  ', 'xx', '  ', 'xx', '7']
szesc = ['xx', '  ', 'xx', '  ', 'xx', '  ', 'xx', '  ', '6']
piec = ['  ', 'xx', '  ', 'xx', '  ', 'xx', '  ', 'xx', '5']
cztery = ['xx', '  ', 'xx', '  ', 'xx', '  ', 'xx', '  ', '4']
trzy = ['  ', 'xx', '  ', 'xx', '  ', 'xx', '  ', 'xx', '3']
dwa = ['xx', '  ', 'xx', '  ', 'xx', '  ', 'xx', '  ', '2']
jeden = ['  ', 'xx', '  ', 'xx', '  ', 'xx', '  ', 'xx', '1']

# Plasza ktora pokazuje polozenie bialych i czarnych pol
podpis1 = [' a', ' b', ' c', ' d', ' e', ' f', ' g', ' h']
osiem1 = ['xx', '  ', 'xx', '  ', 'xx', '  ', 'xx', '  ', '8']
siedem1 = ['  ', 'xx', '  ', 'xx', '  ', 'xx', '  ', 'xx', '7']
szesc1 = ['xx', '  ', 'xx', '  ', 'xx', '  ', 'xx', '  ', '6']
piec1 = ['  ', 'xx', '  ', 'xx', '  ', 'xx', '  ', 'xx', '5']
cztery1 = ['xx', '  ', 'xx', '  ', 'xx', '  ', 'xx', '  ', '4']
trzy1 = ['  ', 'xx', '  ', 'xx', '  ', 'xx', '  ', 'xx', '3']
dwa1 = ['xx', '  ', 'xx', '  ', 'xx', '  ', 'xx', '  ', '2']
jeden1 = ['  ', 'xx', '  ', 'xx', '  ', 'xx', '  ', 'xx', '1']

plansza = [jeden, dwa, trzy, cztery, piec, szesc, siedem, osiem]

plansza1 = [jeden1, dwa1, trzy1, cztery1, piec1, szesc1, siedem1, osiem1]


# funkcja rysujaca plansze
def rysuj():
    print('\n' * 10)  # rysuje 10 pustych lini
    biala = True  # flaga do rysowania na zmiane bialych i czarnych pol

    for i in podpis:  # rysowanie gornego podpisu
        cprint(' ', 'cyan', end='')
        cprint(i, 'cyan', end='')
        cprint(' ', 'cyan', end='')
    cprint('', 'blue', end='\n')


    # glowna petla rysujaca plansze
    for j in range(8):
        for i in plansza[7 - j]:
            if biala:
                #zmienna = cprint(' ', 'cyan', 'on_grey', end='')

                cprint(' ', 'cyan', 'on_grey', end='')
                cprint(i, 'cyan', 'on_grey', end='')
                cprint(' ', 'cyan', 'on_grey', end='')
                biala = False
            else:
                cprint(' ', 'cyan', 'on_green', end='')
                cprint(i, 'cyan', 'on_green', end='')
                cprint(' ', 'cyan', 'on_green', end='')
                biala = True
        cprint('', 'blue', end='\n')

    for i in podpis:  # rysowanie dolnego podpisu
        cprint(' ', 'cyan', end='')
        cprint(i, 'cyan', end='')
        cprint(' ', 'cyan', end='')


# klasa pionek po ktorej dziedzicza wszystkie pozostale bierki
class pionek(object):
    def __init__(self, wiersz, kolumna, kolor, id):
        self.wiersz = wiersz
        self.kolumna = kolumna
        self.kolor = kolor
        self.zbity = False  # czy wciaz znajduje sie na planszy
        self.pierwszy = True  # czy to jego pierwszy ruch, wazne przy pionach, oraz roszadzie

    def ruch(self, wierszCel, kolumnaCel):  # glowna funkcja opisujaca ruch
        ruchy = self.dostepneRuchy(
            False)  # pobiera liste dostepnych ruchow, argument bool mowi o tym czy wyswietlac dane z funkcji czy nie
#TODO Zabezpieczyc przed mozliwoscia ruchu wystawiajacego na szach
        wykonaRuch = False  #flaga ktora mowi o tym czy jest uzytkownik wykonal poprawny ruch
        for ruch in ruchy:
            wierszMozliwosc, kolumnaMozliwosc, bicie, pioneczek = ruch # kazdy ruch sklada sie z pozycji bierki, flagi czy jest mozliwe bice, jesli nie jest to pioneczek jest domyslna figura
            if wierszMozliwosc == wierszCel and kolumnaMozliwosc == kolumnaCel: # sprawdzanie czy ruch zgodny z lista
                wykonaRuch = True


        if wykonaRuch:  #jesli jest mozliwy ruch
            # dziwny, ale specjany warunek dla roszady w lewo
            # roszada jest prowadzona z perspektywy krola
            if kolumnaCel == -6:
                plansza[self.wiersz][2] = self.nazwa # dla roszady w lewo krol zajmuje pole c1
                plansza[self.wiersz][self.kolumna] = plansza1[self.wiersz][self.kolumna] # plansza przyjmuje dla starej lokalizacji domyslne pole biale lub czarne
                self.kolumna = 2 # ustawienie kolumny na 2 tak by krol wiedzial ze jest tam gdzie jest

                #pioneczek jest teraz wiezą
                plansza[pioneczek.wiersz][3] = pioneczek.nazwa #dla roszady w lewo wieża zajmuje pole d1
                plansza[pioneczek.wiersz][pioneczek.kolumna] = plansza1[pioneczek.wiersz][pioneczek.kolumna]
                pioneczek.kolumna = 3
            else:
                # dziwny, ale specjany warunek dla roszady w prawo
                #analogicznie jak wyżej
                if kolumnaCel == -4:
                    plansza[self.wiersz][6] = self.nazwa
                    plansza[self.wiersz][self.kolumna] = plansza1[self.wiersz][self.kolumna]
                    self.kolumna = 6

                    plansza[pioneczek.wiersz][5] = pioneczek.nazwa
                    plansza[pioneczek.wiersz][pioneczek.kolumna] = plansza1[pioneczek.wiersz][pioneczek.kolumna]
                    pioneczek.kolumna = 5

                #Jesli nie ma roszady to nastepuje normalne poruszanie zgodnie z zasadami dla kazdej bierki
                else:

                    plansza[wierszCel][kolumnaCel] = self.nazwa #na planszy w wskazanym miejscu pojawia sie bierka
                    plansza[self.wiersz][self.kolumna] = plansza1[self.wiersz][self.kolumna] # na stare miejsce pojawia sie puste pole
                    self.pierwszy = False   # zostal wykonany pierwszy ruch
                    if bicie:   # jesli jest mozliwe bicie
                        pioneczek.zbity = True # to pioneczek jest zbity
                    self.wiersz = wierszCel #ustawianie odpowiedniego polozenia
                    self.kolumna = kolumnaCel

                    # Sprawdzanie czy nastapi zamiana piona na inna figure takzwana promocja Piona
                    if self.kolor == 'biale' and self.wiersz == 7:
                        #dlugi warunek zastanowic sie czy go nie skrocic
                        if self.nazwa == 'p0' or self.nazwa == 'p1' or self.nazwa == 'p2' or self.nazwa == 'p3' or self.nazwa == 'p4' or self.nazwa == 'p5' or self.nazwa == 'p6' or self.nazwa == 'p7':
                            self.zbity = True #pion sam sie zbija
                            self.nowaBierka() #a my wybieramy nowa bierke

                    if self.kolor == 'czarne' and self.wiersz == 0:
                        if self.nazwa == 'P0' or self.nazwa == 'P1' or self.nazwa == 'P2' or self.nazwa == 'P3' or self.nazwa == 'P4' or self.nazwa == 'P5' or self.nazwa == 'P6' or self.nazwa == 'P7':
                            self.zbity = True
                            self.nowaBierka()

        return wykonaRuch #zwracamy czy wykonalismy ruch czy nie

#TODO Prawdopodobnie tu bedzie musialo byc wstawione sprawdzanie czy ruch wystawia na szach (chyba wystarczy dać flage w czyszach tak by wiedzialo co robic)
    #Funkcja sprawdzajace czy pole na ktore chcemy stanac jest wolne
    def wolnePole(self, wierszCel, kolumnaCel): #przesylamy planową pozycje bierki
        wolne = True
        bicie = False
        pionekDoZbicia = domyslnyPionek # ustawienie domyslnego pionka, inaczej wywala blad jak niczego nie znajdzie
        pole = plansza[wierszCel][kolumnaCel]
        if pole == '  ' or pole == "xx": #jesli pole bylo puste to na pewno jest wolne
            wolne = True
        else: #inaczej trzeba sprawdzic czy na polu znajduje sie bierka ktora mozemy zbic
            wolne = False   #wiemy ze pole nie jest wolne, ale moze mozna go zbic
            nazwaPionka, pioneczek = ktoryPionek(wierszCel, kolumnaCel) #sprawdzamy jaka bierka znajduje sie na planszy w naszym planowym polozeniu

            if pioneczek.kolor == self.kolor: # jesli ten sam kolor to bic nie można
                bicie = False
            else:
                bicie = True
                pionekDoZbicia = pioneczek  # jesli można bić to zapamiętujemy jaką bierkę możemy bić

        #sprawdzanie czy nie powoduje to szacha Olać to na razie jak się wystawi to przegra
       # if wolne or bicie:


        return wolne, bicie, pionekDoZbicia

    #funkcja pozwalajaca wybrac nowa bierkę gdy nastepuje promocja Piona
    def nowaBierka(self):
        while True: #bedzie sie pytał tak długo aż dostanie odpowiedź
            print("jaką chcesz bierkę?")
            print("Hetman: 1")
            print("Skoczek: 2")
            print("Goniec: 3")
            print("Wieza: 4")
            nazwaNowej = int(input())
            #nie znalazłem switch'a wiec takie brzydkie if'y
            if nazwaNowej == 1:
                figury.append(Hetman(self.wiersz, self.kolumna, self.kolor, 1)) #Tworzenie odpowiedniej bierki
                break
            else:
                if nazwaNowej == 2:
                    figury.append(Skoczek(self.wiersz, self.kolumna, self.kolor, 2))
                    break
                else:
                    if nazwaNowej == 3:
                        figury.append(Goniec(self.wiersz, self.kolumna, self.kolor, 2))
                        break
                    else:
                        if nazwaNowej == 4:
                            figury.append(Wieza(self.wiersz, self.kolumna, self.kolor, 2))
                            break
                        else:
                            print("zly klawisz.")


# tworzenie domysknego pionka z skrajnymi wartosciami polozenia i z niepoprawnym kolorem, sluzy on do zapychania miejsca gdy musimy przeslac jakiś pionek do bicia
#pomimo tego ze bicie nie jest mozliwe
#TODO dowiedziec sie jak mozna to ładniej zrobic
domyslnyPionek = pionek(100, 100, 'szary', 500)

#klasa dedykowana dla piona
class pion(pionek):
    def __init__(self, wiersz, kolumna, kolor, id):
        super().__init__(wiersz, kolumna, kolor, id) #dziedziczymy po rodzicu konstruktor
        if self.kolor == 'biale':
            self.nazwa = 'p' + str(id)  #ustawiamy odpowiednia nazwe
            plansza[self.wiersz][self.kolumna] = self.nazwa # oraz wpisujemy jego pozycje na planszy

        if self.kolor == 'czarne':
            self.nazwa = 'P' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

    def dostepneRuchy(self, wyswietlanie): # funkcja zwracajaca liste mozliwych ruchow
        ruchy = []
        # pion ma dostepne 6 ruchy, na prost, na prost x2, i 2x po skosie 2x w przelocie


        #sprawdzic czy po lewej mam pion przeciwnika
        #znajduje sie w odpowiedniej lini: czyli czarne w wierszu 4, biale w wierszu 5
        if (self.kolor=='czarne' and self.wiersz==3) or (self.kolor=='biale' and self.wiersz==4):
            # bicie w przelocie w lewo
            # sprawdzic czy po lewej mam pion przeciwnika
            kolumnaCel=self.kolumna-1
            if(kolumnaCel>=0):
                nazwa, pionekNaCelu = ktoryPionek(self.wiersz, kolumnaCel)
                #jesli bierka po lewej to pionek przeciwnika
                if ((self.kolor=='czarne' and (nazwa =='p0' or nazwa =='p1' or nazwa =='p2' or nazwa =='p3' or nazwa =='p4' or nazwa =='p5' or nazwa =='p6' or nazwa =='p7')) or
                    (self.kolor=='biale' and (nazwa =='P0' or nazwa =='P1' or nazwa =='P2' or nazwa =='P3' or nazwa =='P4' or nazwa =='P5' or nazwa =='P6' or nazwa =='P7'))):
                    #otwieranie listy ruchow
                    plik = open('ruchy.txt', 'r')
                    lista = plik.readlines()
                    plik.close()
                    zmienna = lista.__len__()
                    linia =lista[zmienna - 1]
                    slowa = linia.split()
                    wierszTemp, kolumnaTemp, dobreDane = szachoweNaIndex(slowa[2])
                    nazwaTemp, pionekTemp = ktoryPionek(wierszTemp, kolumnaTemp)
                    if pionekTemp==pionekNaCelu:
                        wierszTemp1, kolumnaTemp1, dobreDane1 = szachoweNaIndex(slowa[1])
                        if (self.kolor=='czarne' and (wierszTemp1+2)==wierszTemp):
                            ruch = [self.wiersz - 1, self.kolumna - 1, True, pionekTemp]
                            ruchy.append(ruch)
                        if (self.kolor == 'biale' and (wierszTemp1 - 2) == wierszTemp):
                            ruch = [self.wiersz + 1, self.kolumna - 1, True, pionekTemp]
                            ruchy.append(ruch)

            # bicie w przelocie w prawo
            # sprawdzic czy po prawej mam pion przeciwnika
            kolumnaCel = self.kolumna +1
            if (kolumnaCel < 8):
                nazwa, pionekNaCelu = ktoryPionek(self.wiersz, kolumnaCel)
                # jesli bierka po lewej to pionek przeciwnika
                if ((self.kolor == 'czarne' and (nazwa == 'p0' or nazwa == 'p1' or nazwa == 'p2' or nazwa == 'p3' or nazwa == 'p4' or nazwa == 'p5' or nazwa == 'p6' or nazwa == 'p7')) or
                        (self.kolor == 'biale' and (nazwa == 'P0' or nazwa == 'P1' or nazwa == 'P2' or nazwa == 'P3' or nazwa == 'P4' or nazwa == 'P5' or nazwa == 'P6' or nazwa == 'P7'))):
                    # otwieranie listy ruchow
                    plik = open('ruchy.txt', 'r')
                    lista = plik.readlines()
                    plik.close()
                    zmienna = lista.__len__()
                    linia = lista[zmienna - 1]
                    slowa = linia.split()
                    wierszTemp, kolumnaTemp, dobreDane = szachoweNaIndex(slowa[2])
                    nazwaTemp, pionekTemp = ktoryPionek(wierszTemp, kolumnaTemp)
                    if pionekTemp == pionekNaCelu:
                        wierszTemp1, kolumnaTemp1, dobreDane1 = szachoweNaIndex(slowa[1])
                        if (self.kolor == 'czarne' and (wierszTemp1 + 2) == wierszTemp):
                            ruch = [self.wiersz - 1, self.kolumna +1, True, pionekTemp]
                            ruchy.append(ruch)
                        if (self.kolor == 'biale' and (wierszTemp1 - 2) == wierszTemp):
                            ruch = [self.wiersz + 1, self.kolumna +1, True, pionekTemp]
                            ruchy.append(ruch)

                        #(self.kolor == 'biale' and (wierszTemp1 - 2) == wierszTemp)):




        # sprawdzono czy pion moze sie poruszyc do przodu o jeden
        if self.kolor == 'biale':
            wierszCel = self.wiersz + 1 #w zaleznosci od koloru pion porusza sie albo w górę, albo w dół
        else:
            wierszCel = self.wiersz - 1

        kolumnaCel = self.kolumna
        wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel) #sprawdzanie czy docelowe pole jest wolne
        if wolne:
            ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
            ruchy.append(ruch)

        # sprawdzono czy pion moze sie poruszyc do przodu o dwa pola
        if self.pierwszy and ruchy.__len__() > 0: #jesli pion nie moze sie poruszyc do przodu o jeden to nie moze tez o dwa, czyli lista ruchow jest pusta
            if self.kolor == 'biale':
                wierszCel = self.wiersz + 2
            else:
                wierszCel = self.wiersz - 2

            kolumnaCel = self.kolumna
            pole = self.wolnePole(wierszCel, kolumnaCel)
            if pole:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)


        #sprawdzanie bicia na skos w lewo
        if self.kolor == 'biale':
            wierszCel = self.wiersz + 1
        else:
            wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna - 1

        if kolumnaCel >= 0: #sprawdzanie czy nie wyskoczyl poza plansze
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if (not wolne) and bicie: #bionek moze dostac sie na docelowe miejsce jedynie gdy robi to przez bicie
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        if self.kolor == 'biale':
            wierszCel = self.wiersz + 1
        else:
            wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna + 1

        if kolumnaCel < 7:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if (not wolne) and bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
        if wyswietlanie:
            print("DOSTEPNE RUCHY")
            for ruch in ruchy:
                wiersz, kolumna, bicie, pionek = ruch
                wierszSzachowo = wiersz + 1
                kolumnaSzachowo = chr(97 + kolumna)
                print(kolumnaSzachowo, wierszSzachowo)

        return ruchy


class Wieza(pionek):
    def __init__(self, wiersz, kolumna, kolor, id):
        super().__init__(wiersz, kolumna, kolor, id)
        if self.kolor == 'biale':
            self.nazwa = 'w' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

        if self.kolor == 'czarne':
            self.nazwa = 'W' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

    def dostepneRuchy(self, wyswietlanie):
        ruchy = []
        # Wieza ma dostep do 4 rodzajow ruchow, na wprost do przodu do tyłu na lewo i prawo
        # print("teraz jestem: ", self.kolor)

        # na wprost w przod
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna
            if wierszCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na wprost w tyl
        for i in range(7):
            wierszCel = self.wiersz - i - 1
            kolumnaCel = self.kolumna
            if wierszCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na lewo
        for i in range(7):
            wierszCel = self.wiersz
            kolumnaCel = self.kolumna - i - 1
            if kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na prawo
        for i in range(7):
            wierszCel = self.wiersz
            kolumnaCel = self.kolumna + i + 1
            if kolumnaCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break
        if wyswietlanie:
            print("DOSTEPNE RUCHY")
            for ruch in ruchy:
                wiersz, kolumna, bicie, pionek = ruch
                wierszSzachowo = wiersz + 1
                kolumnaSzachowo = chr(97 + kolumna)
                print(kolumnaSzachowo, wierszSzachowo)

        return ruchy


class Skoczek(pionek):
    def __init__(self, wiersz, kolumna, kolor, id):
        super().__init__(wiersz, kolumna, kolor, id)
        if self.kolor == 'biale':
            self.nazwa = 's' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

        if self.kolor == 'czarne':
            self.nazwa = 'S' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

    def dostepneRuchy(self, wyswietlanie):
        ruchy = []
        # Wieza ma dostep do 4 rodzajow ruchow, na wprost do przodu do tyłu na lewo i prawo
        # print("teraz jestem: ", self.kolor)


        # na wprost w przod

        wierszCel = self.wiersz + 1
        kolumnaCel = self.kolumna + 2
        if wierszCel < 8 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        # na wprost w tyl

        wierszCel = self.wiersz + 2
        kolumnaCel = self.kolumna + 1
        if wierszCel < 8 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz + 2
        kolumnaCel = self.kolumna - 1
        if wierszCel < 8 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz + 1
        kolumnaCel = self.kolumna - 2
        if wierszCel < 8 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz - 2
        kolumnaCel = self.kolumna + 1
        if wierszCel >= 0 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna + 2
        if wierszCel >= 0 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz - 2
        kolumnaCel = self.kolumna - 1
        if wierszCel >= 0 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna - 2
        if wierszCel >= 0 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
        if wyswietlanie:
            print("DOSTEPNE RUCHY")
            for ruch in ruchy:
                wiersz, kolumna, bicie, pionek = ruch
                wierszSzachowo = wiersz + 1
                kolumnaSzachowo = chr(97 + kolumna)
                print(kolumnaSzachowo, wierszSzachowo)

        return ruchy


class Goniec(pionek):
    def __init__(self, wiersz, kolumna, kolor, id):
        super().__init__(wiersz, kolumna, kolor, id)
        if self.kolor == 'biale':
            self.nazwa = 'g' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

        if self.kolor == 'czarne':
            self.nazwa = 'G' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

    def dostepneRuchy(self, wyswietlanie):
        ruchy = []
        # Wieza ma dostep do 4 rodzajow ruchow, na wprost do przodu do tyłu na lewo i prawo
        # print("teraz jestem: ", self.kolor)

        # na wprost w 45 stopni
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna + i + 1
            if wierszCel < 8 and kolumnaCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na wprost -45 stopni
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna - i - 1
            if wierszCel < 8 and kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # w tył 45 stopni
        for i in range(7):
            wierszCel = self.wiersz - i - 1
            kolumnaCel = self.kolumna + i + 1
            if wierszCel >= 0 and kolumnaCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # w tył -45 stopni
        for i in range(7):
            wierszCel = self.wiersz - i - 1
            kolumnaCel = self.kolumna - i - 1
            if wierszCel >= 0 and kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break
        if wyswietlanie:
            print("DOSTEPNE RUCHY")
            for ruch in ruchy:
                wiersz, kolumna, bicie, pionek = ruch
                wierszSzachowo = wiersz + 1
                kolumnaSzachowo = chr(97 + kolumna)
                print(kolumnaSzachowo, wierszSzachowo)

        return ruchy


class Hetman(pionek):
    def __init__(self, wiersz, kolumna, kolor, id):
        super().__init__(wiersz, kolumna, kolor, id)
        if self.kolor == 'biale':
            self.nazwa = 'h' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

        if self.kolor == 'czarne':
            self.nazwa = 'H' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

    def dostepneRuchy(self, wyswietlanie):
        ruchy = []
        # Wieza ma dostep do 4 rodzajow ruchow, na wprost do przodu do tyłu na lewo i prawo
        # print("teraz jestem: ", self.kolor)


        # na wprost w przod
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna
            if wierszCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na wprost w tyl
        for i in range(7):
            wierszCel = self.wiersz - i - 1
            kolumnaCel = self.kolumna
            if wierszCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na lewo
        for i in range(7):
            wierszCel = self.wiersz
            kolumnaCel = self.kolumna - i - 1
            if kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na prawo
        for i in range(7):
            wierszCel = self.wiersz
            kolumnaCel = self.kolumna + i + 1
            if kolumnaCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na wprost w 45 stopni
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna + i + 1
            if wierszCel < 8 and kolumnaCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na wprost -45 stopni
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna - i - 1
            if wierszCel < 8 and kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # w tył 45 stopni
        for i in range(7):
            wierszCel = self.wiersz - i - 1
            kolumnaCel = self.kolumna + i + 1
            if wierszCel >= 0 and kolumnaCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # w tył -45 stopni
        for i in range(7):
            wierszCel = self.wiersz - i - 1
            kolumnaCel = self.kolumna - i - 1
            if wierszCel >= 0 and kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break
        if wyswietlanie:
            print("DOSTEPNE RUCHY")
            for ruch in ruchy:
                wiersz, kolumna, bicie, pionek = ruch
                wierszSzachowo = wiersz + 1
                kolumnaSzachowo = chr(97 + kolumna)
                print(kolumnaSzachowo, wierszSzachowo)

        return ruchy


class Krol(pionek):
    def __init__(self, wiersz, kolumna, kolor, id):
        super().__init__(wiersz, kolumna, kolor, id)
        if self.kolor == 'biale':
            self.nazwa = 'k' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

        if self.kolor == 'czarne':
            self.nazwa = 'K' + str(id)
            plansza[self.wiersz][self.kolumna] = self.nazwa

    def dostepneRuchy(self, wyswietlanie):
        ruchy = []
        # Wieza ma dostep do 4 rodzajow ruchow, na wprost do przodu do tyłu na lewo i prawo
        # print("teraz jestem: ", self.kolor)


        # na wprost w przod

        wierszCel = self.wiersz + 1
        kolumnaCel = self.kolumna
        if wierszCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        # na wprost w tyl

        wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna
        if wierszCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        # na lewo
        wierszCel = self.wiersz
        kolumnaCel = self.kolumna - 1
        if kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        # na prawo
        wierszCel = self.wiersz
        kolumnaCel = self.kolumna + 1
        if kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        # na wprost w 45 stopni
        wierszCel = self.wiersz + 1
        kolumnaCel = self.kolumna + 1
        if wierszCel < 8 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        # na wprost -45 stopni
        wierszCel = self.wiersz + 1
        kolumnaCel = self.kolumna - 1
        if wierszCel < 8 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)


                # w tył 45 stopni
        wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna + 1
        if wierszCel >= 0 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        # w tył -45 stopni

        wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna - 1
        if wierszCel >= 0 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        # Roszada
        # Sprawdzić czy król nie jest szachowany i czy to jego pierwszy ruch
        szach = CzySzach(self.kolor, False)  # drugi argument mowi o tym czy wyswietlac dane z funkcji
        if (not szach) and self.pierwszy:
            # print('nie ma szachu i pierwszy ruch')
            # sprawdzanie czy nie ma niczego pomiedzy Krolem a Wieza z lewej strony
            if ((plansza[self.wiersz][3] == 'xx' or plansza[self.wiersz][3] == "  ") and
                    (plansza[self.wiersz][2] == 'xx' or plansza[self.wiersz][2] == "  ") and
                    (plansza[self.wiersz][1] == 'xx' or plansza[self.wiersz][1] == "  ")):
                # print('pomiedzy nic nie ma')
                nazwaWieza, Wieza1 = ktoryPionek(self.wiersz, 0)
                # sprawdzanie czy tam stoi Wieza i czy to jej pierwszy ruch
                if (self.kolor == 'biale' and nazwaWieza == 'w0') or (
                        self.kolor == 'czarne' and nazwaWieza == 'W0') and Wieza1.pierwszy:
                    # print('i tak tam jest Wieza')
                    # Jesli jest mozliwa roszada to ruch mowi ze wiersz taki sam, kolumna ROWNA - 77 bicia brak, pioneczek to Wieza do zamiany
                    ruch = [self.wiersz, -6, False, Wieza1]
                    ruchy.append(ruch)

            # sprawdzanie czy nie ma niczego pomiedzy Krolem a Wieza z prawej strony
            if ((plansza[self.wiersz][5] == 'xx' or plansza[self.wiersz][5] == "  ") and
                    (plansza[self.wiersz][6] == 'xx' or plansza[self.wiersz][6] == "  ")):
                nazwaWieza, Wieza1 = ktoryPionek(self.wiersz, 0)
                # sprawdzanie czy tam stoi Wieza i czy to jej pierwszy ruch
                if (self.kolor == 'biale' and nazwaWieza == 'w0') or (
                                self.kolor == 'czarne' and nazwaWieza == 'W0') and Wieza1.pierwszy:
                    # Jesli jest mozliwa roszada to ruch mowi ze wiersz taki sam, kolumna ROWNA - 77 bicia brak, pioneczek to Wieza do zamiany
                    ruch = [self.wiersz, -4, False, Wieza1]
                    ruchy.append(ruch)

        if wyswietlanie:
            print("DOSTEPNE RUCHY")
            for ruch in ruchy:
                wiersz, kolumna, bicie, pionek = ruch
                wierszSzachowo = wiersz + 1
                kolumnaSzachowo = chr(97 + kolumna)
                print(kolumnaSzachowo, wierszSzachowo)

        return ruchy


figury = []




#układ do roszady
'''
for i in range(8):
    figury.append(pion(5, i, 'biale', i))
    # figury.append(pion(6,i,'czarne',i))
    
figury.append(Wieza(0, 0, 'biale', 0))
figury.append(Wieza(0, 7, 'biale', 1))
# figury.append(Wieza(7,0,'czarne',0))
figury.append(Wieza(7, 7, 'czarne', 1))
# figury.append(Skoczek(0,1,'biale',0))
# figury.append(Skoczek(0,6,'biale',1))
figury.append(Skoczek(7, 6, 'czarne', 1))
figury.append(Skoczek(7, 1, 'czarne', 0))
figury.append(Goniec(7, 2, 'czarne', 0))
figury.append(Goniec(7, 5, 'czarne', 1))
# figury.append(Goniec(0,5,'biale',1))
# figury.append(Goniec(0,2,'biale',0))
# figury.append(Hetman(0,3,'biale',0))
figury.append(Hetman(7, 3, 'czarne', 0))
figury.append(Krol(7, 4, 'czarne', 0))
figury.append(Krol(0, 4, 'biale', 0))'''

#układ do bicia w przelocie
# for i in range(8):
#     figury.append(pion(1, i, 'biale', i))
#     #figury.append(pion(6,i,'czarne',i))
#
# figury.append(pion(3,2,'czarne',2))
# figury.append(pion(3,4,'czarne',4))
#
# figury.append(Wieza(0, 0, 'biale', 0))
# figury.append(Wieza(0, 7, 'biale', 1))
# # figury.append(Wieza(7,0,'czarne',0))
# figury.append(Wieza(7, 7, 'czarne', 1))
# # figury.append(Skoczek(0,1,'biale',0))
# # figury.append(Skoczek(0,6,'biale',1))
# figury.append(Skoczek(7, 6, 'czarne', 1))
# figury.append(Skoczek(7, 1, 'czarne', 0))
# figury.append(Goniec(7, 2, 'czarne', 0))
# figury.append(Goniec(7, 5, 'czarne', 1))
# # figury.append(Goniec(0,5,'biale',1))
# # figury.append(Goniec(0,2,'biale',0))
# # figury.append(Hetman(0,3,'biale',0))
# figury.append(Hetman(7, 3, 'czarne', 0))
# figury.append(Krol(7, 4, 'czarne', 0))
# figury.append(Krol(0, 4, 'biale', 0))

#układ normalny
for i in range(8):
    figury.append(pion(1, i, 'biale', i))
    figury.append(pion(6,i,'czarne',i))


figury.append(Wieza(0, 0, 'biale', 0))
figury.append(Wieza(0, 7, 'biale', 1))
figury.append(Wieza(7,0,'czarne',0))
figury.append(Wieza(7, 7, 'czarne', 1))
figury.append(Skoczek(0,1,'biale',0))
figury.append(Skoczek(0,6,'biale',1))
figury.append(Skoczek(7, 6, 'czarne', 1))
figury.append(Skoczek(7, 1, 'czarne', 0))
figury.append(Goniec(7, 2, 'czarne', 0))
figury.append(Goniec(7, 5, 'czarne', 1))
figury.append(Goniec(0,5,'biale',1))
figury.append(Goniec(0,2,'biale',0))
figury.append(Hetman(0,3,'biale',0))
figury.append(Hetman(7, 3, 'czarne', 0))
figury.append(Krol(7, 4, 'czarne', 0))
figury.append(Krol(0, 4, 'biale', 0))



# tu dowane testowe figury
# figury.append(Krol(2,3,'czarne',0))
# figury.append(Skoczek(4,3,'biale',2))
# figury.append(Krol(2,2,'czarne',0))





def ktoryPionek(wiersz, kolumna):
    nazwa = ''
    wybranaFigura = domyslnyPionek
    for pioneczek in figury:
        if pioneczek.kolumna == kolumna and pioneczek.wiersz == wiersz and (not pioneczek.zbity):
            nazwa = pioneczek.nazwa
            wybranaFigura = pioneczek
            break

    return nazwa, wybranaFigura


def szachoweNaIndex(slowo):
    wiersz = 100
    kolumna = 100
    dobreDane = False
    ASCI = [ord(x) for x in slowo]
    # print('To jest ascii: ',ASCI)
    if ASCI.__len__() != 2:
        print("Złe dane")
        return wiersz, kolumna, dobreDane
    else:
        first = int(ASCI[0])  # pozniej zabiezpieczyc przed znakami polskimi
        second = int(ASCI[1])
        # print('a to juz liczby:', first, ' ',second)
        if first > 96 and first < 105 or first == 93 or first == 91:
            kolumna = first - 97
        if first > 64 and first < 73:
            kolumna = first - 65

        if second > 48 and second < 57:
            wiersz = second - 49

    if wiersz < 8 and kolumna < 8:
        dobreDane = True
    else:
        print("Złe dane")

    return wiersz, kolumna, dobreDane


def CzySzach(kolor, wyswietlanie):
    szach = False
    # lista pionkow ktore moga szachowac Krola
    atakujacy = []

    # Wyszukaj gdzie jest król
    for pioneczek in figury:
        if (pioneczek.nazwa == 'k0' or pioneczek.nazwa == 'K0') and pioneczek.kolor == kolor:
            Krol = pioneczek

    # pobranie wszystkich ruchow przeciwnikow
    for pioneczek in figury:
        if pioneczek.kolor != kolor and (not pioneczek.zbity) and (
        not (pioneczek.nazwa == 'k0' or pioneczek.nazwa == 'K0')):
            if wyswietlanie:
                print("Sprawdzam ruch: ", pioneczek.nazwa)
            ruchy = pioneczek.dostepneRuchy(False)
            for ruch in ruchy:
                wiersz, kolumna, bicie, atakowanyPioneczek = ruch
                if atakowanyPioneczek == Krol:  # jesli ktorykolwiek z mozliwych do zaatakowania pionkow jest Krolem wpisz atakujacego na liste atakujacych
                    atakujacy.append(pioneczek)
                    szach = True
    if wyswietlanie:
        print(atakujacy)
    return szach


def szachMat():
    print("ło kurwa ale tu bedzie do pisania")


def Ruszaj():
    kolor = 'biale'
    plik = open('ruchy.txt', 'a')
    plik.write("\n\n\nNOWA PARTIA\n")
    plik.close()
    while True:

        szach = CzySzach(kolor, False)  # drugi argument mowi o tym czy ma sie wyswietlac cos z funkcji
        print("jest szach?: ", szach)
        poprawnyRuch = False
        print("Ruszaja ", kolor)
        rysuj()
        slowo = input('Podaj Pole: ')
        wiersz, kolumna, dobreDane = szachoweNaIndex(slowo)
        if dobreDane:
            # print("wybrałeś taki wiersz i kolumne: ", wiersz,", ",kolumna)
            nazwaPionka, wybranyPionek = ktoryPionek(wiersz, kolumna)
            if nazwaPionka != '':
                if wybranyPionek.kolor == kolor:
                    print("wybrales: ", wybranyPionek.nazwa)
                    wybranyPionek.dostepneRuchy(True)
                    slowoCel = input('Podaj Pole: ')
                    wierszCel, kolumnaCel, dobreDaneCel = szachoweNaIndex(slowoCel)
                    if dobreDane:
                        poprawnyRuch = wybranyPionek.ruch(wierszCel, kolumnaCel)
                        if not poprawnyRuch:
                            print("wybrany pionek nie ma mozliwosci ruchu")
                        else:
                            plik = open('ruchy.txt', 'a')
                            plik.write(kolor+" "+slowo+" "+slowoCel+ "\n")
                            plik.close()
                else:
                    print("Nie możesz sterować pionkiem przeciwnika")
            else:
                print("Nie wybrales pionka.")

        if poprawnyRuch:

            if kolor == 'biale':
                kolor = 'czarne'
            else:
                kolor = 'biale'


# print(ktoryPionek(1,3))

#tworzenie pliku w ktorym zapisywane sa ruchy z rozgrywki
plik = open('ruchy.txt', 'w')
plik.write("Dupa")
Ruszaj()
#plik.close()