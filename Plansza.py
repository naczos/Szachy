from PyQt5.QtWidgets import QWidget, QGraphicsScene, QGraphicsView, QApplication, QGraphicsItem, \
    QStyleOptionGraphicsItem, QPushButton, QGridLayout
from PyQt5 import QtCore
from PyQt5.QtGui import QPixmap
import sys
import xml.dom.minidom
from xml.dom.minidom import Document



#Globalnie Tworzone Pola i Figury


#TODO Bicie w przelocie
#TODO Zapis przebiegu Gry
#TODO dodanie mozliwosci ruchu za pomoca koment z klawiatury
#TODO Granie przez serwer



class Pionek(QGraphicsItem): #Stworzenie klasy pionek z której dziedziczą wszystkie figury
    def __init__(self, wiersz, kolumna, kolor):  # kolor: 1-czarny, 0-bialy
        super(QGraphicsItem, self).__init__()
        self.wiersz = wiersz
        self.kolumna = kolumna
        self.kolor = kolor
        self.wybrany = False
        self.zbity= False
        self.pierwszy = True
        self.puszczony = False #sluży do odznaczania, gdy niechce sie zrobic ruchu

        try:
            if kolor == 1:
                self.pixmap = QPixmap("zdjecia/pion_c.png")
            else:
                self.pixmap = QPixmap("zdjecia/pion_b.png")
        except:
            print("Zle zdjecia")
        #print("wiersz: ", self.wiersz, ' kolumna: ', self.kolumna,  ' kolor: ', self.kolor)
        self.polorzenieX = self.kolumna * self.pixmap.width()
        self.polorzenieY = self.wiersz * self.pixmap.height()
        self.rectF = QtCore.QRectF(self.polorzenieX, self.polorzenieY, self.pixmap.width(), self.pixmap.height())

    def zmienPolorzenie(self):
        self.polorzenieX = self.kolumna * self.pixmap.width()
        self.polorzenieY = self.wiersz * self.pixmap.height()
        self.rectF = QtCore.QRectF(self.polorzenieX, self.polorzenieY, self.pixmap.width(), self.pixmap.height())


    def paint(self, painter, QStyleOptionGraphicsItem, widget=None):

        painter.drawTiledPixmap(self.polorzenieX, self.polorzenieY, self.pixmap.width(), self.pixmap.height(),
                                self.pixmap)

    def boundingRect(self):
        return self.rectF

    def mousePressEvent(self, QGraphicsSceneMouseEvent):
        if (not firstScene.czyJuzKtosAktywny()) and firstScene.tura==self.kolor:
            self.wybrany=True
            wierszSzachowo = self.wiersz + 1
            kolumnaSzachowo = chr(97 + self.kolumna)
            print("Aktywny pion: ",kolumnaSzachowo, wierszSzachowo)
            #podswietlanie dostepnych ruchow
            ruchy = self.dostepneRuchy()
            if len(ruchy) > 0:
                for ruch in ruchy:
                    wierszCel, kolumnaCel, bicie, pioneczek = ruch
                    for wierszyk in firstScene.pola:
                        for pole in wierszyk:
                            if (wierszCel==pole.wiersz and kolumnaCel==pole.kolumna) or (self.wiersz==pole.wiersz and self.kolumna==pole.kolumna):
                                pole.pixmap=QPixmap("zdjecia/pole_z.png")
                            if wierszCel==82 and pole.wiersz==pioneczek.wiersz and pole.kolumna==pioneczek.kolumna:
                                pole.pixmap = QPixmap("zdjecia/pole_z.png")
                firstScene.scene.update()

        if self.puszczony:
            self.wybrany=False
            self.puszczony=False
            wierszSzachowo = self.wiersz + 1
            kolumnaSzachowo = chr(97 + self.kolumna)
            print("Zdezaktywowano pion: ", kolumnaSzachowo, wierszSzachowo)
            ruchy = self.dostepneRuchy()
            if len(ruchy) > 0:
                for ruch in ruchy:
                    wierszCel, kolumnaCel, bicie, pioneczek = ruch
                    for wierszyk in firstScene.pola:
                        for pole in wierszyk:
                            if pole.kolor==1:
                                pole.pixmap = QPixmap("zdjecia/pole_c.png")
                            else:
                                pole.pixmap = QPixmap("zdjecia/pole_b.png")
            firstScene.scene.update()
        firstScene.PrzeniesFigure(self)

    def mouseReleaseEvent(self, QGraphicsSceneMouseEvent):
        if self.wybrany:
            self.puszczony=True

            # Funkcja sprawdzajace czy pole na ktore chcemy stanac jest wolne

    def wolnePole(self, wierszCel, kolumnaCel):  # przesylamy planową pozycje bierki
        wolne = True
        bicie = False
        pionekDoZbicia = self  # ustawienie domyslnego pionka, inaczej wywala blad jak niczego nie znajdzie


        # musze to zrobic innaczej, wolne bedzie wtedy gdy zadna figura nie znajduje sie w tym miejscu
        for figura in firstScene.figury:
            if figura.wiersz == wierszCel and figura.kolumna == kolumnaCel:
                wolne = False
                if figura.kolor != self.kolor:
                    bicie = True
                    pionekDoZbicia = figura

        return wolne, bicie, pionekDoZbicia

    def dostepneRuchy(self):  # funkcja zwracajaca liste mozliwych ruchow
        ruchy = []
        return ruchy


class Pion(Pionek):
    def __init__(self, wiersz, kolumna, kolor):
        super().__init__(wiersz, kolumna, kolor) #dziedziczymy po rodzicu konstruktor
        try:
            if kolor == 1:
                self.pixmap = QPixmap("zdjecia/pion_c.png")
            else:
                self.pixmap = QPixmap("zdjecia/pion_b.png")
        except:
            print("Zle zdjecia")
        self.nazwa='pion'
        #print("wiersz: ", self.wiersz, ' kolumna: ', self.kolumna, ' nazwa: ', self.nazwa, ' kolor: ', self.kolor)



    def dostepneRuchy(self): # funkcja zwracajaca liste mozliwych ruchow
        ruchy = []
        # pion ma dostepne 6 ruchy, na prost, na prost x2, i 2x po skosie 2x w przelocie


        # sprawdzono czy pion moze sie poruszyc do przodu o jeden
        if self.kolor == 0:
            wierszCel = self.wiersz + 1 #w zaleznosci od koloru pion porusza sie albo w górę, albo w dół
        else:
            wierszCel = self.wiersz - 1

        kolumnaCel = self.kolumna
        wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel) #sprawdzanie czy docelowe pole jest wolne
        if wolne:
            ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
            ruchy.append(ruch)

        # sprawdzono czy pion moze sie poruszyc do przodu o dwa pola
        if self.pierwszy and ruchy.__len__() > 0: #jesli pion nie moze sie poruszyc do przodu o jeden to nie moze tez o dwa, czyli lista ruchow jest pusta
            if self.kolor == 0:
                wierszCel = self.wiersz + 2
            else:
                wierszCel = self.wiersz - 2

            kolumnaCel = self.kolumna
            pole = self.wolnePole(wierszCel, kolumnaCel)
            if pole:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
        #
        #
        #sprawdzanie bicia na skos w lewo
        if self.kolor == 0:
            wierszCel = self.wiersz + 1
        else:
            wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna - 1

        if kolumnaCel >= 0: #sprawdzanie czy nie wyskoczyl poza plansze
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if (not wolne) and bicie: #bionek moze dostac sie na docelowe miejsce jedynie gdy robi to przez bicie
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        if self.kolor == 0:
            wierszCel = self.wiersz + 1
        else:
            wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna + 1

        if kolumnaCel < 7:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if (not wolne) and bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        print("DOSTEPNE RUCHY")
        for ruch in ruchy:
            wiersz, kolumna, bicie, pionek = ruch
            wierszSzachowo = wiersz + 1
            kolumnaSzachowo = chr(97 + kolumna)
            print(kolumnaSzachowo, wierszSzachowo)

        return ruchy


class Wieza(Pionek):
    def __init__(self, wiersz, kolumna, kolor):
        super().__init__(wiersz, kolumna, kolor) #dziedziczymy po rodzicu konstruktor
        try:
            if kolor == 1:
                self.pixmap = QPixmap("zdjecia/wieza_c.png")
            else:
                self.pixmap = QPixmap("zdjecia/wieza_b.png")
        except:
            print("Zle zdjecia")
        self.nazwa='wieza'

    def dostepneRuchy(self): # funkcja zwracajaca liste mozliwych ruchow
        ruchy = []
        # Wieza ma dostep do 4 rodzajow ruchow, na wprost do przodu do tyłu na lewo i prawo
        # print("teraz jestem: ", self.kolor)

        # na wprost w przod
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna
            if wierszCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na wprost w tyl
        for i in range(7):
            wierszCel = self.wiersz - i - 1
            kolumnaCel = self.kolumna
            if wierszCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na lewo
        for i in range(7):
            wierszCel = self.wiersz
            kolumnaCel = self.kolumna - i - 1
            if kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na prawo
        for i in range(7):
            wierszCel = self.wiersz
            kolumnaCel = self.kolumna + i + 1
            if kolumnaCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        print("DOSTEPNE RUCHY")
        for ruch in ruchy:
            wiersz, kolumna, bicie, pionek = ruch
            wierszSzachowo = wiersz + 1
            kolumnaSzachowo = chr(97 + kolumna)
            print(kolumnaSzachowo, wierszSzachowo)

        return ruchy

class Skoczek(Pionek):
    def __init__(self, wiersz, kolumna, kolor):
        super().__init__(wiersz, kolumna, kolor) #dziedziczymy po rodzicu konstruktor
        try:
            if kolor == 1:
                self.pixmap = QPixmap("zdjecia/skoczek_c.png")
            else:
                self.pixmap = QPixmap("zdjecia/skoczek_b.png")
        except:
            print("Zle zdjecia")
        self.nazwa='skoczek'

    def dostepneRuchy(self):  # funkcja zwracajaca liste mozliwych ruchow
        ruchy = []
        # Wieza ma dostep do 4 rodzajow ruchow, na wprost do przodu do tyłu na lewo i prawo
        # print("teraz jestem: ", self.kolor)


        # na wprost w przod

        wierszCel = self.wiersz + 1
        kolumnaCel = self.kolumna + 2
        if wierszCel < 8 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        # na wprost w tyl

        wierszCel = self.wiersz + 2
        kolumnaCel = self.kolumna + 1
        if wierszCel < 8 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz + 2
        kolumnaCel = self.kolumna - 1
        if wierszCel < 8 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz + 1
        kolumnaCel = self.kolumna - 2
        if wierszCel < 8 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz - 2
        kolumnaCel = self.kolumna + 1
        if wierszCel >= 0 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna + 2
        if wierszCel >= 0 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz - 2
        kolumnaCel = self.kolumna - 1
        if wierszCel >= 0 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna - 2
        if wierszCel >= 0 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)
            else:
                if bicie:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)

        print("DOSTEPNE RUCHY")
        for ruch in ruchy:
            wiersz, kolumna, bicie, pionek = ruch
            wierszSzachowo = wiersz + 1
            kolumnaSzachowo = chr(97 + kolumna)
            print(kolumnaSzachowo, wierszSzachowo)

        return ruchy

class Goniec(Pionek):
    def __init__(self, wiersz, kolumna, kolor):
        super().__init__(wiersz, kolumna, kolor) #dziedziczymy po rodzicu konstruktor
        try:
            if kolor == 1:
                self.pixmap = QPixmap("zdjecia/goniec_c.png")
            else:
                self.pixmap = QPixmap("zdjecia/goniec_b.png")
        except:
            print("Zle zdjecia")
        self.nazwa='goniec'

    def dostepneRuchy(self):  # funkcja zwracajaca liste mozliwych ruchow
        ruchy = []
        # Wieza ma dostep do 4 rodzajow ruchow, na wprost do przodu do tyłu na lewo i prawo
        # print("teraz jestem: ", self.kolor)

        # na wprost w 45 stopni
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna + i + 1
            if wierszCel < 8 and kolumnaCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na wprost -45 stopni
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna - i - 1
            if wierszCel < 8 and kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # w tył 45 stopni
        for i in range(7):
            wierszCel = self.wiersz - i - 1
            kolumnaCel = self.kolumna + i + 1
            if wierszCel >= 0 and kolumnaCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # w tył -45 stopni
        for i in range(7):
            wierszCel = self.wiersz - i - 1
            kolumnaCel = self.kolumna - i - 1
            if wierszCel >= 0 and kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break
        print("DOSTEPNE RUCHY")
        for ruch in ruchy:
            wiersz, kolumna, bicie, pionek = ruch
            wierszSzachowo = wiersz + 1
            kolumnaSzachowo = chr(97 + kolumna)
            print(kolumnaSzachowo, wierszSzachowo)

        return ruchy

class Hetman(Pionek):
    def __init__(self, wiersz, kolumna, kolor):
        super().__init__(wiersz, kolumna, kolor) #dziedziczymy po rodzicu konstruktor
        try:
            if kolor == 1:
                self.pixmap = QPixmap("zdjecia/hetman_c.png")
            else:
                self.pixmap = QPixmap("zdjecia/hetman_b.png")
        except:
            print("Zle zdjecia")
        self.nazwa='hetman'

    def dostepneRuchy(self):  # funkcja zwracajaca liste mozliwych ruchow
        ruchy = []
        # Wieza ma dostep do 4 rodzajow ruchow, na wprost do przodu do tyłu na lewo i prawo
        # print("teraz jestem: ", self.kolor)


        # na wprost w przod
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna
            if wierszCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na wprost w tyl
        for i in range(7):
            wierszCel = self.wiersz - i - 1
            kolumnaCel = self.kolumna
            if wierszCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na lewo
        for i in range(7):
            wierszCel = self.wiersz
            kolumnaCel = self.kolumna - i - 1
            if kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na prawo
        for i in range(7):
            wierszCel = self.wiersz
            kolumnaCel = self.kolumna + i + 1
            if kolumnaCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na wprost w 45 stopni
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna + i + 1
            if wierszCel < 8 and kolumnaCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # na wprost -45 stopni
        for i in range(7):
            wierszCel = self.wiersz + i + 1
            kolumnaCel = self.kolumna - i - 1
            if wierszCel < 8 and kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # w tył 45 stopni
        for i in range(7):
            wierszCel = self.wiersz - i - 1
            kolumnaCel = self.kolumna + i + 1
            if wierszCel >= 0 and kolumnaCel < 8:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break

        # w tył -45 stopni
        for i in range(7):
            wierszCel = self.wiersz - i - 1
            kolumnaCel = self.kolumna - i - 1
            if wierszCel >= 0 and kolumnaCel >= 0:
                wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
                if wolne:
                    ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                    ruchy.append(ruch)
                else:
                    if bicie:
                        ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                        ruchy.append(ruch)
                    break
            else:
                break
        print("DOSTEPNE RUCHY")
        for ruch in ruchy:
            wiersz, kolumna, bicie, pionek = ruch
            wierszSzachowo = wiersz + 1
            kolumnaSzachowo = chr(97 + kolumna)
            print(kolumnaSzachowo, wierszSzachowo)

        return ruchy

class Krol(Pionek):
    def __init__(self, wiersz, kolumna, kolor):
        super().__init__(wiersz, kolumna, kolor) #dziedziczymy po rodzicu konstruktor
        try:
            if kolor == 1:
                self.pixmap = QPixmap("zdjecia/krol_c.png")
            else:
                self.pixmap = QPixmap("zdjecia/krol_b.png")
        except:
            print("Zle zdjecia")
        self.nazwa='krol'

    def dostepneRuchy(self):  # funkcja zwracajaca liste mozliwych ruchow
        ruchy = []
        # Wieza ma dostep do 4 rodzajow ruchow, na wprost do przodu do tyłu na lewo i prawo
        # print("teraz jestem: ", self.kolor)


        # na wprost w przod

        wierszCel = self.wiersz + 1
        kolumnaCel = self.kolumna
        if wierszCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        # na wprost w tyl

        wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna
        if wierszCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        # na lewo
        wierszCel = self.wiersz
        kolumnaCel = self.kolumna - 1
        if kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        # na prawo
        wierszCel = self.wiersz
        kolumnaCel = self.kolumna + 1
        if kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        # na wprost w 45 stopni
        wierszCel = self.wiersz + 1
        kolumnaCel = self.kolumna + 1
        if wierszCel < 8 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        # na wprost -45 stopni
        wierszCel = self.wiersz + 1
        kolumnaCel = self.kolumna - 1
        if wierszCel < 8 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)


                # w tył 45 stopni
        wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna + 1
        if wierszCel >= 0 and kolumnaCel < 8:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        # w tył -45 stopni

        wierszCel = self.wiersz - 1
        kolumnaCel = self.kolumna - 1
        if wierszCel >= 0 and kolumnaCel >= 0:
            wolne, bicie, pioneczek = self.wolnePole(wierszCel, kolumnaCel)
            if wolne or bicie:
                ruch = [wierszCel, kolumnaCel, bicie, pioneczek]
                ruchy.append(ruch)

        # # Roszada
        wiezaBL=self
        wiezaBP=self
        wiezaCL=self
        wiezaCP=self
        wieza=self

        #wyszuka wieze
        for figura in firstScene.figury:
            if figura.nazwa=='wieza' and figura.wiersz==0 and figura.kolumna==0:
                wiezaBL = figura

            if figura.nazwa == 'wieza' and figura.wiersz == 0 and figura.kolumna == 7:
                wiezaBP=figura
            if figura.nazwa == 'wieza' and figura.wiersz == 7 and figura.kolumna == 0:
                wiezaCL=figura
            if figura.nazwa == 'wieza' and figura.wiersz == 7 and figura.kolumna == 7:
                wiezaCP=figura

        #sprawdzić pierwszy ruch
        if self.pierwszy and wiezaBP.pierwszy and self.kolor == wiezaBP.kolor:
            #sprawdzic czy nie ma nic pomiedzy
            wolne, bicie, pionekDoZbicia=self.wolnePole(self.wiersz, 5)
            wolne1, bicie1, pionekDoZbicia1=self.wolnePole(self.wiersz, 6)
            if wolne and wolne1:
                ruch = [82, 80, False, wiezaBP]
                ruchy.append(ruch)

        if self.pierwszy and wiezaCP.pierwszy and self.kolor == wiezaCP.kolor:
            # sprawdzic czy nie ma nic pomiedzy
            wolne, bicie, pionekDoZbicia = self.wolnePole(self.wiersz, 5)
            wolne1, bicie1, pionekDoZbicia1 = self.wolnePole(self.wiersz, 6)
            if wolne and wolne1:
                ruch = [82, 80, False, wiezaCP]
                ruchy.append(ruch)

        if self.pierwszy and wiezaBL.pierwszy and self.kolor == wiezaBL.kolor:
            # sprawdzic czy nie ma nic pomiedzy
            wolne, bicie, pionekDoZbicia = self.wolnePole(self.wiersz, 1)
            wolne1, bicie1, pionekDoZbicia1 = self.wolnePole(self.wiersz, 2)
            wolne2, bicie1, pionekDoZbicia1 = self.wolnePole(self.wiersz, 3)
            if wolne and wolne1 and wolne2:
                ruch = [82, 76, False, wiezaBL]
                ruchy.append(ruch)

        if self.pierwszy and wiezaBL.pierwszy and self.kolor == wiezaCL.kolor:
            # sprawdzic czy nie ma nic pomiedzy
            wolne, bicie, pionekDoZbicia = self.wolnePole(self.wiersz, 1)
            wolne1, bicie1, pionekDoZbicia1 = self.wolnePole(self.wiersz, 2)
            wolne2, bicie1, pionekDoZbicia1 = self.wolnePole(self.wiersz, 3)
            if wolne and wolne1 and wolne2:
                ruch = [82, 76, False, wiezaCL]
                ruchy.append(ruch)



        # # Sprawdzić czy król nie jest szachowany i czy to jego pierwszy ruch
        # szach = CzySzach(self.kolor, False)  # drugi argument mowi o tym czy wyswietlac dane z funkcji
        # if (not szach) and self.pierwszy:
        #     # print('nie ma szachu i pierwszy ruch')
        #     # sprawdzanie czy nie ma niczego pomiedzy Krolem a Wieza z lewej strony
        #     if ((plansza[self.wiersz][3] == 'xx' or plansza[self.wiersz][3] == "  ") and
        #             (plansza[self.wiersz][2] == 'xx' or plansza[self.wiersz][2] == "  ") and
        #             (plansza[self.wiersz][1] == 'xx' or plansza[self.wiersz][1] == "  ")):
        #         # print('pomiedzy nic nie ma')
        #         nazwaWieza, Wieza1 = ktoryPionek(self.wiersz, 0)
        #         # sprawdzanie czy tam stoi Wieza i czy to jej pierwszy ruch
        #         if (self.kolor == 'biale' and nazwaWieza == 'w0') or (
        #                         self.kolor == 'czarne' and nazwaWieza == 'W0') and Wieza1.pierwszy:
        #             # print('i tak tam jest Wieza')
        #             # Jesli jest mozliwa roszada to ruch mowi ze wiersz taki sam, kolumna ROWNA - 77 bicia brak, pioneczek to Wieza do zamiany
        #             ruch = [self.wiersz, -6, False, Wieza1]
        #             ruchy.append(ruch)
        #
        #     # sprawdzanie czy nie ma niczego pomiedzy Krolem a Wieza z prawej strony
        #     if ((plansza[self.wiersz][5] == 'xx' or plansza[self.wiersz][5] == "  ") and
        #             (plansza[self.wiersz][6] == 'xx' or plansza[self.wiersz][6] == "  ")):
        #         nazwaWieza, Wieza1 = ktoryPionek(self.wiersz, 0)
        #         # sprawdzanie czy tam stoi Wieza i czy to jej pierwszy ruch
        #         if (self.kolor == 'biale' and nazwaWieza == 'w0') or (
        #                         self.kolor == 'czarne' and nazwaWieza == 'W0') and Wieza1.pierwszy:
        #             # Jesli jest mozliwa roszada to ruch mowi ze wiersz taki sam, kolumna ROWNA - 77 bicia brak, pioneczek to Wieza do zamiany
        #             ruch = [self.wiersz, -4, False, Wieza1]
        #             ruchy.append(ruch)


        print("DOSTEPNE RUCHY")
        for ruch in ruchy:
            wiersz, kolumna, bicie, pionek = ruch
            wierszSzachowo = wiersz + 1
            kolumnaSzachowo = chr(97 + kolumna)
            print(kolumnaSzachowo, wierszSzachowo)

        return ruchy


class Pole(QGraphicsItem):
    def __init__(self, wiersz, kolumna, kolor): #kolor: 1-czarny, 0-bialy
        super(QGraphicsItem, self).__init__()
        self.wiersz=wiersz
        self.kolumna=kolumna
        self.kolor=kolor
        self.setAcceptHoverEvents(True)
        try:
            if kolor==1:
                self.pixmap = QPixmap("zdjecia/pole_c.png")
            else:
                self.pixmap = QPixmap("zdjecia/pole_b.png")
        except:
            print("Zle zdjecia")

        self.polorzenieX = self.kolumna * self.pixmap.width()
        self.polorzenieY = self.wiersz * self.pixmap.height()
        self.rectF = QtCore.QRectF(self.polorzenieX, self.polorzenieY, self.pixmap.width(), self.pixmap.height())


    def paint(self, painter, QStyleOptionGraphicsItem, widget = None):

        painter.drawTiledPixmap(self.polorzenieX, self.polorzenieY, self.pixmap.width(), self.pixmap.height(), self.pixmap)

    def boundingRect(self):
        return self.rectF

    # def hoverEnterEvent(self, event):


    def mousePressEvent(self, QGraphicsSceneMouseEvent):
        firstScene.PrzeniesFigure(self)





#Tworzenie planszy na ktorej beda staly wszystkie pola i pionki
class Plansza(QGraphicsScene):
    def __init__(self, pola, figury):
        super(QGraphicsScene, self).__init__()
        self.pola=pola
        self.figury = figury
        self.Pokaz()



    def Pokaz(self):
        for wiersz in self.pola:
            for pole in wiersz:
                self.addItem(pole)
        for figura in self.figury:
            self.addItem(figura)

    def Zniszcz(self):
        for wiersz in self.pola:
            for pole in wiersz:
                self.removeItem(pole)
        for figura in self.figury:
            self.removeItem(figura)

    def update(self, *__args):
        self.Zniszcz()
        self.Pokaz()






class MyFirstScene(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.klawiszowo=False
        self.tura=0 #jesli o to ruszaja biale jesli 1 to czarne
        # Tworzenie planszy do gry z 64 polami
        self.pola = []

        self.doc = Document()
        self.node = self.doc.createElement('foo')
        self.doc.appendChild(self.node)


        ##OTwieranie listy ruchów i zapis do listaRuchów
        self.listaRuchow=[]
        self.indexDoListyRuchow=0

        dom = xml.dom.minidom.parse("my_xml.xml")
        Foo = dom.getElementsByTagName('foo')
        i = 0
        for node in Foo:
            alist = node.getElementsByTagName('ruch')
            for a in alist:
                Title = a.firstChild.data
                self.listaRuchow.append(Title)

        for i in range(8):
            wiersz = []
            for j in range(8):
                if (j + i) % 2 == 0:
                    wiersz.append(Pole(i, j, 0))
                else:
                    wiersz.append(Pole(i, j, 1))
            self.pola.append(wiersz)

        # dodanie pionkow
        self.figury = []
        for i in range(8):
            self.figury.append(Pion(1, i, 0))
            self.figury.append(Pion(6, i, 1))
        self.figury.append(Wieza(7, 0, 1))
        self.figury.append(Wieza(7, 7, 1))
        self.figury.append(Wieza(0, 0, 0))
        self.figury.append(Wieza(0, 7, 0))
        self.figury.append(Skoczek(7, 1, 1))
        self.figury.append(Skoczek(7, 6, 1))
        self.figury.append(Skoczek(0, 1, 0))
        self.figury.append(Skoczek(0, 6, 0))
        self.figury.append(Goniec(7, 2, 1))
        self.figury.append(Goniec(7, 5, 1))
        self.figury.append(Goniec(0, 2, 0))
        self.figury.append(Goniec(0, 5, 0))
        self.figury.append(Hetman(7, 3, 1))
        self.figury.append(Hetman(0, 3, 0))
        self.figury.append(Krol(7, 4, 1))
        self.figury.append(Krol(0, 4, 0))



        self.scene = Plansza(self.pola,self.figury)





        self.view = QGraphicsView(self.scene, self)
        self.view.resize(400, 400)
        self.show()

    def make_xml(self, ruch):
        node1 = self.doc.createElement('ruch')
        node1.appendChild(self.doc.createTextNode(ruch))
        self.node.appendChild(node1)


    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_F5:
            with open("my_xml.xml", 'w') as xml_file:
                self.doc.writexml(xml_file, addindent='\t', newl='\n')
            self.close()



        if e.key() == QtCore.Qt.Key_Plus:
            self.klawiszowo=True



            foo=self.listaRuchow[self.indexDoListyRuchow]
            self.indexDoListyRuchow=self.indexDoListyRuchow+1
            wybor, wybor1 = str(foo).split()

            wiersz, kolumna, dobreDane = self.szachoweNaIndex(wybor)


            if dobreDane:
                print("oto index: ", wiersz, kolumna)
                for figura in self.figury:
                    if figura.wiersz==wiersz and figura.kolumna==kolumna:
                        print("oto nazwa: ", figura.nazwa)
                        if (not self.czyJuzKtosAktywny()) and self.tura == figura.kolor:
                            figura.wybrany = True
                            wierszSzachowo = figura.wiersz + 1
                            kolumnaSzachowo = chr(97 + figura.kolumna)
                            print("Aktywny pion: ", kolumnaSzachowo, wierszSzachowo)
                            # podswietlanie dostepnych ruchow
                            ruchy = figura.dostepneRuchy()
                            if len(ruchy) > 0:
                                for ruch in ruchy:
                                    wierszCel, kolumnaCel, bicie, pioneczek = ruch
                                    for wierszyk in self.pola:
                                        for pole in wierszyk:
                                            if (wierszCel == pole.wiersz and kolumnaCel == pole.kolumna) or (
                                                    figura.wiersz == pole.wiersz and figura.kolumna == pole.kolumna):
                                                pole.pixmap = QPixmap("zdjecia/pole_z.png")
                                            if wierszCel == 82 and pole.wiersz == pioneczek.wiersz and pole.kolumna == pioneczek.kolumna:
                                                pole.pixmap = QPixmap("zdjecia/pole_z.png")
                                self.scene.update()

                        if figura.puszczony:
                            figura.wybrany = False
                            figura.puszczony = False
                            wierszSzachowo = figura.wiersz + 1
                            kolumnaSzachowo = chr(97 + figura.kolumna)
                            print("Zdezaktywowano pion: ", kolumnaSzachowo, wierszSzachowo)
                            ruchy = figura.dostepneRuchy()
                            if len(ruchy) > 0:
                                for ruch in ruchy:
                                    wierszCel, kolumnaCel, bicie, pioneczek = ruch
                                    for wierszyk in self.pola:
                                        for pole in wierszyk:
                                            if pole.kolor == 1:
                                                pole.pixmap = QPixmap("zdjecia/pole_c.png")
                                            else:
                                                pole.pixmap = QPixmap("zdjecia/pole_b.png")
                            self.scene.update()
                        figura.puszczony = True
                        self.PrzeniesFigure(figura)

            #wybor = input('Wybierz pole: ')

            wiersz, kolumna, dobreDane = self.szachoweNaIndex(wybor1)

            if dobreDane:
                print("oto index: ", wiersz, kolumna)
                for wiersze in self.pola:
                    for pole in wiersze:
                        if pole.wiersz == wiersz and pole.kolumna == kolumna:
                            self.PrzeniesFigure(pole)


    def szachoweNaIndex(self, slowo):
        wiersz = 100
        kolumna = 100
        dobreDane = False
        ASCI = [ord(x) for x in slowo]
        # print('To jest ascii: ',ASCI)
        if ASCI.__len__() != 2:
            print("Złe dane")
            return wiersz, kolumna, dobreDane
        else:
            first = int(ASCI[0])  # pozniej zabiezpieczyc przed znakami polskimi
            second = int(ASCI[1])
            # print('a to juz liczby:', first, ' ',second)
            if first > 96 and first < 105 or first == 93 or first == 91:
                kolumna = first - 97
            if first > 64 and first < 73:
                kolumna = first - 65

            if second > 48 and second < 57:
                wiersz = second - 49

        if wiersz < 8 and kolumna < 8:
            dobreDane = True
        else:
            print("Złe dane")

        return wiersz, kolumna, dobreDane

    def PrzeniesFigure(self, pole):
        for figura in self.figury:
            if figura.wybrany and figura.kolor==self.tura:
                #print("oto wiersz w ktorym jest figura: ", figura.wiersz)
                ruchy = figura.dostepneRuchy()
                #
                if len(ruchy)>0:
                     for ruch in ruchy:
                         wierszCel, kolumnaCel, bicie, pioneczek = ruch

                         #do ogarniecia roszady

                         if wierszCel == 82:
                             # wykonanie roszady gdy klikniece na odpowiednia wieze
                             if pole.wiersz == pioneczek.wiersz and pole.kolumna == pioneczek.kolumna and pioneczek.kolor==self.tura:
                                 if kolumnaCel == 76:
                                     figura.kolumna = 2  # krol skacze
                                     pioneczek.kolumna = 3  # wieza skacze

                                 if kolumnaCel == 80:
                                     figura.kolumna = 6  # krol skacze
                                     pioneczek.kolumna = 5  # wieza skacze


                                 figura.zmienPolorzenie()
                                 pioneczek.zmienPolorzenie()
                                 figura.wybrany = False
                                 figura.pierwszy = False
                                 pioneczek.wybrany = False
                                 pioneczek.pierwszy = False
                                 print("Krol ", figura.kolor, 'Wykonal roszade w ',  chr(kolumnaCel))

                                 self.tura = 1 - self.tura
                                 for wierszyk in self.pola:
                                     for pole in wierszyk:
                                         if pole.kolor == 1:
                                             pole.pixmap = QPixmap("zdjecia/pole_c.png")
                                         else:
                                             pole.pixmap = QPixmap("zdjecia/pole_b.png")
                                 self.scene.update()

                #
                         if wierszCel==pole.wiersz and kolumnaCel==pole.kolumna:
                             if bicie:
                                 pioneczek.zbity=True
                                 self.figury.remove(pioneczek)

                             wierszSzachowo = figura.wiersz + 1
                             kolumnaSzachowo = chr(97 + figura.kolumna)

                             figura.wiersz = pole.wiersz
                             figura.kolumna = pole.kolumna
                             figura.zmienPolorzenie()
                             figura.wybrany = False
                             figura.pierwszy=False


                             wierszSzachowoCel = pole.wiersz + 1
                             kolumnaSzachowoCel = chr(97 + pole.kolumna)
                             print(kolumnaSzachowo, wierszSzachowo, 'ruszył sie na ', kolumnaSzachowoCel, wierszSzachowoCel)
                             ruchXML = kolumnaSzachowo+str(wierszSzachowo)+" "+kolumnaSzachowoCel+str(wierszSzachowoCel)
                             self.make_xml(ruchXML)

                             self.tura = 1 - self.tura
                             for wierszyk in self.pola:
                                 for pole in wierszyk:
                                     if pole.kolor == 1:
                                         pole.pixmap = QPixmap("zdjecia/pole_c.png")
                                     else:
                                         pole.pixmap = QPixmap("zdjecia/pole_b.png")
                             self.scene.update()



    def ZwrocFigury(self):
        print("kurwa mac")
        return self.figury

    def czyJuzKtosAktywny(self):
        ktosAktywny=False
        for figura in self.figury:
            if figura.wybrany:
                ktosAktywny=True
        return ktosAktywny

    def ktoryPionek(self, wiersz, kolumna):
        nazwa = ''
        wybranaFigura = self.figury[0]
        for pioneczek in self.figury:
            if pioneczek.kolumna == kolumna and pioneczek.wiersz == wiersz and (not pioneczek.zbity):
                nazwa = pioneczek.nazwa
                wybranaFigura = pioneczek
                break

        return nazwa, wybranaFigura


if __name__=="__main__":
    #domyslnyPionek = Pionek(1, 2, 1)  # wstawiony domyslny pionek, potrzebny przy algorytmie preszukiwania
    app = QApplication(sys.argv)
    firstScene = MyFirstScene()
    sys.exit(app.exec_())