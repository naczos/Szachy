//=========================================================
//                   PROJEKT Z JAVA                   
//=========================================================

import java.applet.Applet;
import java.awt.event.*;
import com.sun.j3d.utils.applet.MainFrame;
import com.sun.j3d.utils.behaviors.vp.OrbitBehavior;
import com.sun.j3d.utils.geometry.Cylinder;
import com.sun.j3d.utils.universe.*;
import javax.media.j3d.*;
import javax.vecmath.*;
import com.sun.j3d.utils.image.TextureLoader;
import javax.swing.JPanel;
import javax.swing.Timer;
import com.sun.j3d.utils.geometry.Box;
import com.sun.j3d.utils.geometry.Sphere;
import javax.vecmath.Point3d;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.Queue;
import javax.swing.JLabel;


public class robot extends Applet implements ActionListener, KeyListener 
{
 //=============================================================
 //  trancformacje obiektów
 //===========================================================

private SimpleUniverse simpleU = null;
private OrbitBehavior orbit = null;//sterowanie myszką

    
private BranchGroup wezel_scena = null;
private BranchGroup scena = null;
private BranchGroup  branch_pudlo = null;
private BranchGroup  branch_pudlo1 = null;
    
 private TransformGroup pozycja;
 private TransformGroup pozycjaRamienia;
 private TransformGroup rotacjaRamienia;
 private TransformGroup pozycjaPrzedramienia;
 private TransformGroup rotacjaPrzedramienia;
 private TransformGroup przesuniecieWału_GD;
 private TransformGroup pozycjaWału;
 private TransformGroup pret_; 
 private TransformGroup pudelko;
 private TransformGroup pudelko1; 

 
 private Button przyciskStart = new Button("Start");   //Uruchomienie robota
 private Button przyciskStop = new Button("Stop"); //opis sterowania
 
 private Transform3D trans = new Transform3D();
 private Transform3D rotacja_1 = new Transform3D();
 private Transform3D rotacja_2 = new Transform3D();
 private Transform3D przesuniecie_3 = new Transform3D();
 private Transform3D przesuniecie_pret_ = new Transform3D();
 private Transform3D przesuniecie_pudla = new Transform3D();
 private Transform3D przesuniecie_pudla1 = new Transform3D();
 private Transform3D przesuniecie_obserwatora = new Transform3D();
 
 private Vector3f positionObject_pudlo_n=null;
 private Vector3f positionObject_pudlo_1n=null;

//definiowanie obiektów
 private Cylinder pret = null;
 private Box pudlo = null;
 private Box pudlo1 = null;
  
 
 //zmienne globalne
private float height=0.0f;
private float sign=1.0f;
private Timer zegar_1;
private Timer zegar_2;
private float xloc=0.0f;
private float yloc=0.0f;
private double kat_1=0,kat_2=0, kat_1n, kat_2n;
private float przes_3=0, przes_3n;
private boolean klawisz_a=false, klawisz_s=false, klawisz_d=false, pierwsza_spacja = true, odtwarzanie_pierwsze=false;
private boolean klawisz_w=false, klawisz_q=false, klawisz_e=false, klawisz_space = false, w_pudle=false, nagrywanie_pierwsze=true;
private boolean podniesiony=false, spada=false, podniesiony1=false, spada1=false, nagrywanie=false, odtwarzanie=false;

//Zapis ruchów
Queue<Integer> ruch = new LinkedList<Integer>();

   
 public robot() 
 {
       
       setLayout(new BorderLayout());
       GraphicsConfiguration config = SimpleUniverse.getPreferredConfiguration(); //odczyt konfiguracji
       Canvas3D canvas3D = new Canvas3D(config);
       add("Center",canvas3D);
       canvas3D.addKeyListener(this);
       
       zegar_1 = new Timer(10,this);     // częstość odświerzania pozycji
       zegar_2 = new Timer(1000,this);     // częstość odczytywania klawiszy

       JPanel panel = new JPanel();      // Jpanel utworzony w górnej części okna
       JLabel label1 = new JLabel("Q/E przedramie, A/D ramie, W i S góra/dół, R/P nagrywanie/odtwarzanie");
       panel.add(label1);
       panel.add(przyciskStart);
       panel.add(przyciskStop);
       add("" + "North",panel);          // lokalizacja panela
       
       przyciskStart.addActionListener(this);
       przyciskStart.addKeyListener(this);

       scena = utworzScene();
       scena.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE); //ustawienie uprawnień do dodawaia dzieci
       scena.setCapability(BranchGroup.ALLOW_CHILDREN_READ);
       scena.setCapability(TransformGroup.ALLOW_CHILDREN_EXTEND);
       scena.compile();
       
       simpleU = new SimpleUniverse(canvas3D);

       BoundingSphere bounds = new BoundingSphere(new Point3d(0,0,0),100);

       //=============================================================================
       //  Obserwator
       //=============================================================================
        
       //sterowanie obserwatorem
       OrbitBehavior orbit = new OrbitBehavior(canvas3D, OrbitBehavior.REVERSE_ALL); //sterowanie myszką
       orbit.setMinRadius(0);
       orbit.setSchedulingBounds(bounds);
       ViewingPlatform vp = simpleU.getViewingPlatform();
       vp.setViewPlatformBehavior(orbit);
       przesuniecie_obserwatora.set(new Vector3f(0.0f,0.5f,3.0f)); //odległość obserwatora (-1.2f,1.5f,2.0f

       simpleU.getViewingPlatform().getViewPlatformTransform().setTransform(przesuniecie_obserwatora);
       simpleU.addBranchGraph(scena);
    }//KONIEC robot
 
 
//przyciski sterujące robotem
    public void keyPressed(KeyEvent e)
    {
        if (e.getKeyChar()=='d') {klawisz_d=true;}
        if (e.getKeyChar()=='a') {klawisz_a=true;}
        if (e.getKeyChar()=='w') {klawisz_w=true;}
        if (e.getKeyChar()=='s') {klawisz_s=true;}
        if (e.getKeyChar()=='q') {klawisz_q=true;}
        if (e.getKeyChar()=='e') {klawisz_e=true;}
        if (e.getKeyChar()==' ') {klawisz_space=true;}
        //ustawianie zatrzasków na nagrywanie i odtwarzanie
        if (e.getKeyChar()=='r') {nagrywanie=true; odtwarzanie=false; odtwarzanie_pierwsze=true; nagrywanie_pierwsze=true;} 
        if (e.getKeyChar()=='p') {nagrywanie=false; odtwarzanie=true;}
    }
    public void keyReleased(KeyEvent e) {
        if (e.getKeyChar()=='a') {klawisz_a=false;}
        if (e.getKeyChar()=='d') {klawisz_d=false;}
        if (e.getKeyChar()=='w') {klawisz_w=false;}
        if (e.getKeyChar()=='s') {klawisz_s=false;}
        if (e.getKeyChar()=='q') {klawisz_q=false;}
        if (e.getKeyChar()=='e') {klawisz_e=false;}
        if (e.getKeyChar()==' ') {klawisz_space=false; pierwsza_spacja=true;}
    }
    public void keyTyped(KeyEvent e) {}


    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==przyciskStart)
        {
            if(!zegar_1.isRunning()) {zegar_1.start();} //jeżeli naciśnięto "start" - uruchomienie zegara 1
        } else 
        {

         //=================================
         //  obsługa klawiszy
         //=================================
         
         //sprawdzanie czy koncowka jest w pudelku
          Transform3D transform = new Transform3D();
          Vector3f position = new Vector3f();
          pret.getLocalToVworld(transform); 
          transform.get(position);
          
          //lokalizacja pudelka
          Transform3D transformObject = new Transform3D(); 
          Vector3f positionObject = new Vector3f();
          pudlo.getLocalToVworld(transformObject); 
          transformObject.get(positionObject);
          
          //lokalizacja pudelka1
          Transform3D transformObject1 = new Transform3D(); 
          Vector3f positionObject1 = new Vector3f();
          pudlo1.getLocalToVworld(transformObject1); 
          transformObject1.get(positionObject1);
          
          if((position.x>positionObject1.x-(pudlo1.getXdimension()+pret.getRadius())  &&   position.x<(positionObject1.x+(pudlo1.getXdimension()+pret.getRadius()))  
                  &&   position.z>positionObject1.z-(pudlo1.getZdimension()+pret.getRadius())   &&   position.z<positionObject1.z+(pudlo1.getZdimension()+pret.getRadius())   
                  &&   position.y<=(2*pudlo1.getYdimension()+pret.getHeight()/2))
                  || (position.x>positionObject.x-(pudlo.getXdimension()+pret.getRadius())  &&   position.x<positionObject.x+(pudlo.getXdimension()+pret.getRadius()) 
                  &&   position.z>positionObject.z-(pudlo.getZdimension()+pret.getRadius())   &&   position.z<positionObject.z+(pudlo.getZdimension()+pret.getRadius())  
                  &&   position.y<=(2*pudlo1.getYdimension()+pret.getHeight()/2)))
          {
              w_pudle=true;
          }else w_pudle=false;
         
         if(!odtwarzanie)
         {
            if(kat_1 > -2.65) // końcowe granice ramienia
            {
                if(klawisz_d==true && w_pudle==false) //ruch dozwolony jesli jest wcisnięty klawisz oraz pręt nie jest w obiekcie
                {
                    kat_1=kat_1-0.03; 
                    if(nagrywanie) ruch.add(1); // gdy wszystkie warunki spełnione zapisujemy ruch
                }
            }
            if(kat_1 < 2.65)
            {
                if(klawisz_a==true && w_pudle==false)
                {
                    kat_1=kat_1+0.03;
                    if(nagrywanie) ruch.add(2);
                }
            }
            if(kat_2 > -2.65)
            {
                if(klawisz_e==true && w_pudle==false)
                {
                    kat_2=kat_2-0.04;
                    if(nagrywanie) ruch.add(3);
                }
            }
            if(kat_2 < 2.65)
            {
                if(klawisz_q==true && w_pudle==false)
                {
                    kat_2=kat_2+0.04;
                    if(nagrywanie) ruch.add(4);
                }
            }
                
            if(w_pudle)
            {
                if(-pret.getHeight()/2+2*pudlo.getYdimension() < przes_3)
                {
                    if(klawisz_s==true)
                    {
                        przes_3=przes_3-0.005f;
                        if(nagrywanie) ruch.add(5);
                    }
                }
            }else
            {
                if(-pret.getHeight()/2 < przes_3)
                {
                   
                    if(klawisz_s==true)
                    {
                        przes_3=przes_3-0.005f;
                        if(nagrywanie) ruch.add(5);
                    }
                }
            }
            
            
            if(przes_3 < 0.3f){
                if(klawisz_w==true)
                {
                    przes_3=przes_3+0.005f;
                    if(nagrywanie) ruch.add(6);
                }
            }
            if(klawisz_space)
            {
                if(pierwsza_spacja)
                {
                    pierwsza_spacja=false;
                    if(nagrywanie) ruch.add(7);
                   
                    podnies();
                }
            }
         } 
            //jesli bedzie nagrywanie
            if(nagrywanie)
            {
                if(nagrywanie_pierwsze)
                {
                    kat_1n=kat_1;
                    kat_2n=kat_2;
                    przes_3n=przes_3;
                    
                    Transform3D transformObject_pudlo_n = new Transform3D(); 
                    positionObject_pudlo_n = new Vector3f();
                    pudlo.getLocalToVworld(transformObject_pudlo_n); 
                    transformObject_pudlo_n.get(positionObject_pudlo_n);
                    
                    Transform3D transformObject_pudlo_1n = new Transform3D(); 
                    positionObject_pudlo_1n = new Vector3f();
                    pudlo1.getLocalToVworld(transformObject_pudlo_1n); 
                    transformObject_pudlo_1n.get(positionObject_pudlo_1n);
                    
                    
                    nagrywanie_pierwsze=false;
                }
            }
            
            
            //jesli będzie otwarzanie
            if(odtwarzanie)
            {
                 if(odtwarzanie_pierwsze)
                 {
                    // ustawianie warunków z początku 
                  kat_1=kat_1n;
                  kat_2=kat_2n;
                  przes_3=przes_3n;
                  przesuniecie_pudla1.set(positionObject_pudlo_1n); //(x,ile nad ziemią,y)
                  pudelko1.setTransform(przesuniecie_pudla1);
                  przesuniecie_pudla.set(positionObject_pudlo_n); //(x,ile nad ziemią,y)
                  pudelko.setTransform(przesuniecie_pudla);
                  odtwarzanie_pierwsze=false;
                 }
                 
                if(!ruch.isEmpty()){ //kolejka zapisu ruchu
                    switch (ruch.poll())
                    {
                        case 1:
                            kat_1 = kat_1-0.03;
                            break;
                        case 2:
                            kat_1 = kat_1+0.03;
                            break;
                        case 3:
                            kat_2 = kat_2-0.04;
                            break;
                        case 4:
                            kat_2 = kat_2+0.04;
                            break;
                        case 5:
                            przes_3 = przes_3 - 0.005f;
                            break;
                        case 6:
                             przes_3 = przes_3 + 0.005f;
                            break;
                         case 7:
                            podnies();
                            break;
                        default:
                            break;
                     }
                }else // jesli kolejka jest pusta
                 {
                    odtwarzanie=false;
                 }
            }
            
            
            //---------------------------------
            //poruszanie robotem
            //-----------------------------------
            
            rotacja_1.rotY(kat_1);
            rotacjaRamienia.setTransform(rotacja_1);

            rotacja_2.rotY(kat_2);
            rotacjaPrzedramienia.setTransform(rotacja_2);

            przesuniecie_3.setTranslation(new Vector3f(0f, przes_3,0f));
            pozycjaWału.setTransform(przesuniecie_3);
            
            //========================
              //    Grawitacja
            //======================
            
            //spada pudlo
            if(spada){ //spadek
           
                //Pobieranie położenia klocka
                Transform3D transformObject_spad = new Transform3D();
                pudlo.getLocalToVworld(transformObject_spad);
                Vector3f position_spad = new Vector3f(); 
                transformObject_spad.get(position_spad);
                //zmienianie wartości y położenia klocka
                position_spad.setY(-0.01f);
                position_spad.setX(0);
                position_spad.setZ(0);

                Transform3D trans = new Transform3D();
                trans.set(position_spad);
                przesuniecie_pudla.mul(transformObject_spad, trans);   //przesunięcie odłączonego obiektu w nowe miejsce
                pudelko.setTransform(przesuniecie_pudla);

                 //do pomiaru wysokosci na ktorej w tej chwili znajduje sie pudlo
                Transform3D transformPomiar = new Transform3D(); 
                Vector3f positionPomiar = new Vector3f();
                pudlo.getLocalToVworld(transformPomiar); 
                transformPomiar.get(positionPomiar);
                //jesli wykrył ziemie to przestaje spadać
                if(positionPomiar.y<=pudlo.getYdimension()) spada=false;
            
            }//koniec if(spada)
            
            //spada pudlo1
            if(spada1){ //spadek
           
                Transform3D transformObject_spad = new Transform3D();
                pudlo1.getLocalToVworld(transformObject_spad);
                Vector3f position_spad = new Vector3f();
                transformObject_spad.get(position_spad);
                //zmienianie wartości y położenia klocka
                position_spad.setY(-0.01f);
                position_spad.setX(0);
                position_spad.setZ(0);

                Transform3D trans = new Transform3D();
                trans.set(position_spad);
                przesuniecie_pudla1.mul(transformObject_spad, trans);   //przesunięcie odłączonego obiektu w nowe miejsce
                pudelko1.setTransform(przesuniecie_pudla1);

                //do pomiaru wysokosci na ktorej w tej chwili znajduje sie pudlo1
                Transform3D transformPomiar = new Transform3D(); 
                Vector3f positionPomiar = new Vector3f();
                pudlo1.getLocalToVworld(transformPomiar); 
                transformPomiar.get(positionPomiar);
                //jesli wykrył ziemie to przestaje spadać
                if(positionPomiar.y<=pudlo1.getYdimension()) spada1=false;
            
            }//koniec if(spada1)
            
        } // koniec jesli nie przycisk
    } //KONIEC actionPerformed
    
    
    
    //========================
    //    podnoszenie klocka
    //======================
    void podnies()
    {
        //?????
        // PODNOSZENIE PUDLA
        //?????
      if(!podniesiony)
      {

        //położenie koncowki preta
        Transform3D transform = new Transform3D();
        Vector3f position = new Vector3f();
        pret.getLocalToVworld(transform); 
        transform.get(position);

        //lokalizacja pudelka
        Transform3D transformObject = new Transform3D(); 
        Vector3f positionObject = new Vector3f();
        pudlo.getLocalToVworld(transformObject); 
        transformObject.get(positionObject);

         
         //sprawdzanie warunku podniesienia
        if(position.x>positionObject.x-(pudlo.getXdimension()+pret.getRadius()-0.02f)  &&   position.x<(positionObject.x+(pudlo.getXdimension()+pret.getRadius()-0.02f))  
        &&   position.z>positionObject.z-(pudlo.getZdimension()+pret.getRadius()-0.02f)   &&   position.z<positionObject.z+(pudlo.getZdimension()+pret.getRadius()-0.02f)   
        &&   position.y<=(2*pudlo.getYdimension()+pret.getHeight()/2))            
        {
                
            podniesiony=true;
            przesuniecie_pudla.set(new Vector3f(0,-0.4f,0));
            pudelko.setTransform(przesuniecie_pudla);
                    
            scena.removeChild(branch_pudlo); // przerywanie dziedziczenia Brachgroup w którym jest pudło
            pret_.addChild(branch_pudlo); //dodanie Brachgroup w którym jest pudło do chwytaka
            spada=false;
        }   
          
      }else // jesli jest juz podniesiony
      {
            podniesiony=false;
            Transform3D transform = new Transform3D(); 
            Vector3f position = new Vector3f();
            pudlo.getLocalToVworld(transform); //sprawdzenie pozycji obiektu
            transform.get(position); // przekazanie tej lokalizacji do position
            pret_.removeChild(branch_pudlo); // przerywanie dziedziczenia Brachgroup w którym jest pudło z chwytakiem
            scena.addChild(branch_pudlo); //dodanie Brachgroup w którym jest pudło do sceny
           
            position.setY(-0);
            position.setX(0);
            position.setZ(0);
            Transform3D trans = new Transform3D();
            trans.set(position);
            przesuniecie_pret_.mul(transform, trans);   //przesunięcie odłączonego obiektu w nowe miejsce
            pudelko.setTransform(przesuniecie_pret_);

            spada=true;
      }// koniec podniesiony pudlo
      
        //?????
        // PODNOSZENIE Pudla1
        //?????
      if(!podniesiony1)
      {
          
        //położenie koncowki preta
        Transform3D transform = new Transform3D();
        Vector3f position = new Vector3f();
        pret.getLocalToVworld(transform); 
        transform.get(position);

        //lokalizacja pudelka
        Transform3D transformObject = new Transform3D(); 
        Vector3f positionObject = new Vector3f();
        pudlo1.getLocalToVworld(transformObject); 
        transformObject.get(positionObject);
        
        //sprawdzanie warunku podniesienia
        if(position.x>positionObject.x-(pudlo1.getXdimension()+pret.getRadius()-0.02f)  &&   position.x<(positionObject.x+(pudlo1.getXdimension()+pret.getRadius()-0.02f))  
            &&   position.z>positionObject.z-(pudlo1.getZdimension()+pret.getRadius()-0.02f)   &&   position.z<positionObject.z+(pudlo1.getZdimension()+pret.getRadius()-0.02f)   
            &&   position.y<=(2*pudlo1.getYdimension()+pret.getHeight()/2))
        {
            podniesiony1=true;
            przesuniecie_pudla1.set(new Vector3f(0,-0.4f,0));
            pudelko1.setTransform(przesuniecie_pudla1);
           
            scena.removeChild(branch_pudlo1);
            pret_.addChild(branch_pudlo1); //dodanie obiektu do chwytaka
            spada1=false;
        }   
    
      }else // jesli już podniesiony
      {
        podniesiony1=false;
        Transform3D transform = new Transform3D(); 
        Vector3f position = new Vector3f();    
        pudlo1.getLocalToVworld(transform); //sprawdzenie pozycji obiektu
        transform.get(position);
        pret_.removeChild(branch_pudlo1);
        scena.addChild(branch_pudlo1);
        
        position.setY(-0);
        position.setX(0);
        position.setZ(0);
        Transform3D trans = new Transform3D();
        trans.set(position);
        przesuniecie_pret_.mul(transform, trans);   //przesunięcie odłączonego obiektu w nowe miejsce
        pudelko1.setTransform(przesuniecie_pret_);
        
        spada1=true;
      }// koniec podniesiony pudlo 1
      
        
        
    }//koniec podnies
  
    //========================
    //    WYGLĄD I SŁADOWE ROBOTA
    //======================
    public BranchGroup utworzScene() 
    {

        wezel_scena = new BranchGroup();
        
        
        //==============================
        //      Oświetlenie sceny
        //==============================
        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0,0.0,0.0),100);
        Color3f light1Color = new Color3f(0.2f,0.3f,0.5f); //kolor 
        Vector3f light1Direction = new Vector3f(4.0f,7.0f,-12.0f); //kierunek
        DirectionalLight light1 = new DirectionalLight(light1Color, light1Direction);
        light1.setInfluencingBounds(bounds);
        wezel_scena.addChild(light1); //dodanie do sceny

        //dodanie światła bezkierunkowego
        Color3f ambientColor = new Color3f(0.8f,0.9f,0.4f);
        AmbientLight ambientLightNode = new AmbientLight(ambientColor);
        ambientLightNode.setInfluencingBounds(bounds);
        wezel_scena.addChild(ambientLightNode);
        
        
        
        Appearance wyglad_pret = new Appearance();
        
    
        Appearance wyglad_niebo = new Appearance();
        Appearance wyglad_podloga = new Appearance();



        //------------------------------------
        //wyglad Podstawka
        //--------------------------------------
        Appearance wyglad_podstawka = new Appearance();
        Material metalowy = new Material();
        metalowy.setEmissiveColor( 0.1f, 0.2f, 0.2f );
        metalowy.setDiffuseColor( 0.7f, 0.5f, 0.7f);
        metalowy.setSpecularColor( 0.2f, 0.2f, 0.2f );
        metalowy.setShininess(200f);   
        wyglad_podstawka.setMaterial(metalowy);
        
        
        //------------------------------------
        //wyglad Ramion
        //--------------------------------------
        Appearance wyglad_ramie = new Appearance();
        TextureLoader loader5 = new TextureLoader("obrazki/plastik3.jpg",null);
        ImageComponent2D image5 = loader5.getImage();
        Texture2D ramiaczko = new Texture2D(Texture.BASE_LEVEL, Texture.RGBA,
                                        image5.getWidth(), image5.getHeight());
        ramiaczko.setImage(0, image5);
        ramiaczko.setBoundaryModeS(Texture.WRAP);
        ramiaczko.setBoundaryModeT(Texture.WRAP);
        wyglad_ramie.setTexture(ramiaczko);

        
        //---------------------------------
        // wygląd preta i trzonu
        //------------------------ 
        TextureLoader loader4 = new TextureLoader("obrazki/metal3.jpg",null);
        ImageComponent2D image4 = loader4.getImage();
        Texture2D precik = new Texture2D(Texture.BASE_LEVEL, Texture.RGBA,
                                        image4.getWidth(), image4.getHeight());
        precik.setImage(0, image4);
        precik.setBoundaryModeS(Texture.WRAP);
        precik.setBoundaryModeT(Texture.WRAP);
        wyglad_pret.setTexture(precik);
        
         
        //--------------------------------
        // wyglad Niebo
        //--------------------------------
        TextureLoader loader2 = new TextureLoader("obrazki/kosmos3.jpg",null);
        ImageComponent2D image2 = loader2.getImage();
        Texture2D chmury = new Texture2D(Texture.BASE_LEVEL, Texture.RGBA,
                                        image2.getWidth(), image2.getHeight());
        chmury.setImage(0, image2);
        chmury.setBoundaryModeS(Texture.WRAP);
        chmury.setBoundaryModeT(Texture.WRAP);
        wyglad_niebo.setTexture(chmury);
        
        
        //--------------------------------
        // wyglad Podloga
        //--------------------------------
        TextureLoader loader3 = new TextureLoader("obrazki/księżyc.jpg",null);
        ImageComponent2D image3 = loader3.getImage();
        Texture2D kafle = new Texture2D(Texture.BASE_LEVEL, Texture.RGBA,
                                        image3.getWidth(), image3.getHeight());
        kafle.setImage(0, image3);
        kafle.setBoundaryModeS(Texture.WRAP);
        kafle.setBoundaryModeT(Texture.WRAP);
        wyglad_podloga.setTexture(kafle);
        
        //--------------------------------
        // Rozmieszczenie elementów
        //--------------------------------
        pozycja = new TransformGroup();
        wezel_scena.addChild(pozycja);
        
        //Cylinder (trzon)
        TransformGroup wieza_p = new TransformGroup();
        Transform3D przesuniecie_wiezy = new Transform3D();
        przesuniecie_wiezy.set(new Vector3f(0.0f,0.3f,0.0f));
        wieza_p.setTransform(przesuniecie_wiezy);  
        Cylinder walec = new Cylinder(0.055f,0.6f, Cylinder.GENERATE_NORMALS| Cylinder.GENERATE_TEXTURE_COORDS, wyglad_pret); //trzon
        wieza_p.addChild(walec);
        pozycja.addChild(wieza_p);
        
        
        //Ramie Pierwsze
        rotacjaRamienia = new TransformGroup();
        pozycjaRamienia = new TransformGroup();
        Transform3D przesuniecie_pozycja_1 = new Transform3D();
        przesuniecie_pozycja_1.set(new Vector3f(0.0f,0.6f,0.0f)); //położenie ramienia
        pozycjaRamienia.setTransform(przesuniecie_pozycja_1);
            
        rotacjaRamienia.setCapability( TransformGroup.ALLOW_TRANSFORM_WRITE);//dodane
        rotacjaRamienia.setCapability( TransformGroup.ALLOW_TRANSFORM_READ);//dodane
        pozycjaRamienia.addChild(rotacjaRamienia);
        pozycja.addChild(pozycjaRamienia);
        
        //ramię_1
        TransformGroup ramie_1 = new TransformGroup();
        Transform3D przesuniecie_ramie_1 = new Transform3D();
        przesuniecie_ramie_1.set(new Vector3f(0.0f,0.0f,0.22f));
        ramie_1.setTransform(przesuniecie_ramie_1);
        Box ramie1 = new Box(0.06f,0.018f,0.22f,Box.GENERATE_NORMALS|Box.GENERATE_TEXTURE_COORDS, wyglad_ramie);  //1-szerokość
        
        //łaczenie ramienia pierwszego z trzonem
        TransformGroup nakladka_11 = new TransformGroup();
        Transform3D przesuniecie_nakladka_11 = new Transform3D();
        przesuniecie_nakladka_11.set(new Vector3f(0.0f,0.00f,-0.22f));
        nakladka_11.setTransform(przesuniecie_nakladka_11);
        Cylinder nakladka11 = new Cylinder(0.0601f,0.036f,Cylinder.GENERATE_NORMALS|Cylinder.GENERATE_TEXTURE_COORDS,wyglad_ramie); // tuż przy trzonie
        nakladka_11.addChild(nakladka11);
        ramie_1.addChild(nakladka_11);
        
        //łaczenie ramienia pierwszego z ramieniem drugim
        TransformGroup nakladka_12 = new TransformGroup();
        Transform3D przesuniecie_nakladka_12 = new Transform3D();
        przesuniecie_nakladka_12.set(new Vector3f(0.0f,0.0f,0.22f));
        nakladka_12.setTransform(przesuniecie_nakladka_12);
        Cylinder nakladka12 = new Cylinder(0.06f,0.036f,Cylinder.GENERATE_NORMALS|Cylinder.GENERATE_TEXTURE_COORDS, wyglad_ramie);
        nakladka_12.addChild(nakladka12);
        ramie_1.addChild(nakladka_12);

        ramie_1.addChild(ramie1);
        rotacjaRamienia.addChild(ramie_1);
        
        
        //Ramie Drugie
        rotacjaPrzedramienia = new TransformGroup();
        pozycjaPrzedramienia = new TransformGroup();
        Transform3D przesuniecie_pozycja_2 = new Transform3D();
        przesuniecie_pozycja_2.set(new Vector3f(0.0f,0.036f,0.44f)); //położenie przedramienia
        pozycjaPrzedramienia.setTransform(przesuniecie_pozycja_2);
    
        rotacjaPrzedramienia.setCapability( TransformGroup.ALLOW_TRANSFORM_WRITE);
        rotacjaPrzedramienia.setCapability( TransformGroup.ALLOW_TRANSFORM_READ);
        pozycjaPrzedramienia.addChild(rotacjaPrzedramienia);
        rotacjaRamienia.addChild(pozycjaPrzedramienia);
        
        //ramię 2
        TransformGroup ramie_2 = new TransformGroup();
        Transform3D przesuniecie_ramie_2 = new Transform3D();
        przesuniecie_ramie_2.set(new Vector3f(0.0f,0.0f,0.22f));
        ramie_2.setTransform(przesuniecie_ramie_2);
        Box ramie2 = new Box(0.06f,0.018f,0.22f,Box.GENERATE_NORMALS|Box.GENERATE_TEXTURE_COORDS, wyglad_ramie);
        
        //łaczenie ramienia drugiego z ramieniem pierwszym
        TransformGroup nakladka_21 = new TransformGroup();
        Transform3D przesuniecie_nakladka_21 = new Transform3D();
        przesuniecie_nakladka_21.set(new Vector3f(0.0f,0.00f,-0.22f));
        nakladka_21.setTransform(przesuniecie_nakladka_21);
        Cylinder nakladka21 = new Cylinder(0.0601f,0.036f,Cylinder.GENERATE_NORMALS|Cylinder.GENERATE_TEXTURE_COORDS, wyglad_ramie);
        nakladka_21.addChild(nakladka21);
        ramie_2.addChild(nakladka_21);
        
        //łaczenie ramienia drugiego z chwytakiem
        TransformGroup nakladka_22 = new TransformGroup();
        Transform3D przesuniecie_nakladka_22 = new Transform3D();
        przesuniecie_nakladka_22.set(new Vector3f(0.0f,0.0f,0.22f));
        nakladka_22.setTransform(przesuniecie_nakladka_22);
        Cylinder nakladka22 = new Cylinder(0.06f,0.036f,Cylinder.GENERATE_NORMALS|Cylinder.GENERATE_TEXTURE_COORDS, wyglad_ramie);
        nakladka_22.addChild(nakladka22);
        ramie_2.addChild(nakladka_22);
        
        ramie_2.addChild(ramie2);
        rotacjaPrzedramienia.addChild(ramie_2);
        
        //Chwytak
        pozycjaWału = new TransformGroup();
        przesuniecieWału_GD = new TransformGroup();
        Transform3D przesuniecie_pozycja_3 = new Transform3D();
        przesuniecie_pozycja_3.set(new Vector3f(0.0f,0.0f,0.44f));
        przesuniecieWału_GD.setTransform(przesuniecie_pozycja_3);
            
        pozycjaWału.setCapability( TransformGroup.ALLOW_TRANSFORM_WRITE);
        pozycjaWału.setCapability( TransformGroup.ALLOW_TRANSFORM_READ);
        przesuniecieWału_GD.addChild(pozycjaWału);
        rotacjaPrzedramienia.addChild(przesuniecieWału_GD);
        
        //Wał podnoszący
        pret_ = new TransformGroup();
        pret_.setCapability(TransformGroup.ALLOW_CHILDREN_EXTEND);
        pret_.setCapability(TransformGroup.ALLOW_CHILDREN_WRITE);

        przesuniecie_pret_.set(new Vector3f(0.0f,0.0f,0.0f));
        pret_.setTransform(przesuniecie_pret_);
        pret = new Cylinder(0.025f,0.65f, Cylinder.GENERATE_NORMALS| Cylinder.GENERATE_TEXTURE_COORDS, wyglad_pret); //wymiary pręta
        pret_.addChild(pret);
        pozycjaWału.addChild(pret_);
        
        
        //pudełko Pierwsze
        branch_pudlo = new BranchGroup();
        branch_pudlo.setCapability(BranchGroup.ALLOW_DETACH);
  
        pudelko = new TransformGroup();
        pudelko.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        
        przesuniecie_pudla.set(new Vector3f(0.0f,0.08f,0.5f)); //(x,ile nad ziemią,y)
        pudelko.setTransform(przesuniecie_pudla);

        pudlo = new Box(0.08f,0.08f,0.08f,Box.GENERATE_NORMALS|Box.GENERATE_TEXTURE_COORDS,wyglad_podloga);//wymiary pudla
        pudelko.addChild(pudlo);
        
        branch_pudlo.addChild(pudelko);
        wezel_scena.addChild(branch_pudlo);
        
        //pudełko Drugie
        branch_pudlo1 = new BranchGroup();
        branch_pudlo1.setCapability(BranchGroup.ALLOW_DETACH);
        
        pudelko1 = new TransformGroup();
        pudelko1.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        
        przesuniecie_pudla1.set(new Vector3f(0.5f,0.08f,0.5f)); //(x,ile nad ziemią,y)
        pudelko1.setTransform(przesuniecie_pudla1);

        pudlo1 = new Box(0.08f,0.08f,0.08f,Box.GENERATE_NORMALS|Box.GENERATE_TEXTURE_COORDS,wyglad_podloga);//wymiary pudla
        pudelko1.addChild(pudlo1);
        
        branch_pudlo1.addChild(pudelko1);
        wezel_scena.addChild(branch_pudlo1);
        
        //podstawa
        TransformGroup podstawa = new TransformGroup();
        Transform3D przesuniecie_sciany = new Transform3D();
        przesuniecie_sciany.set(new Vector3f(0.0f,0.015f,0.0f));
        podstawa.setTransform(przesuniecie_sciany);

        Box sciana = new Box(0.12f,0.022f,0.12f,Box.GENERATE_NORMALS|Box.GENERATE_TEXTURE_COORDS|Box.GENERATE_NORMALS_INWARD,wyglad_ramie);//wymiary podstawki
        podstawa.addChild(sciana);
        pozycja.addChild(podstawa);
        
       
        //-------------------
        //Otoczenie Robota
        //-------------------
        //Niebo jako wnętrze ogromnej sfery
        Sphere sky = new Sphere(10f, Sphere.GENERATE_NORMALS_INWARD| Sphere.GENERATE_TEXTURE_COORDS|Sphere.GENERATE_NORMALS, wyglad_niebo); //tworzenie nieba z tekstura do srodka
        wezel_scena.addChild(sky);
        
        //Podłoga
        TransformGroup podloga_TG = new TransformGroup();
        Transform3D przesuniecie_podloga = new Transform3D();
        przesuniecie_podloga.set(new Vector3f(0.0f,-0.01f,0.0f));
        podloga_TG.setTransform(przesuniecie_podloga);
        
        Cylinder podloga = new Cylinder(10f,0.0001f,Cylinder.GENERATE_NORMALS| Cylinder.GENERATE_TEXTURE_COORDS, wyglad_podloga);
        podloga_TG.addChild(podloga);
        wezel_scena.addChild(podloga_TG);
              
        return wezel_scena;
    }
    //obsługiwanie ramienia

    public static void main(String[] args) {
        robot bb = new robot();
        bb.addKeyListener(bb);
        MainFrame mf = new MainFrame(bb,900,500); // wielkość okna programu
    }

}//END klas robot